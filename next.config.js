/** @type {import('next').NextConfig} */

const shortid = require('shortid')

const { i18n } = require('./next-i18next.config')

const BUILD_ID = shortid()

const withCustomExtends = (
  providedExports = {}
) => {
  return {
    ...providedExports,
    reactStrictMode: true,
    i18n,
    generateBuildId: async () => BUILD_ID,
    images: {
      domains: [
        'i.scdn.co',
        'mosaic.scdn.co',
        'open.scdn.co',
      ],
    },
  }
}

module.exports = withCustomExtends()
