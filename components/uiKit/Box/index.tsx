import classNames from 'classnames'
import React from 'react'

export type TBoxProps = React.HTMLAttributes<HTMLDivElement> & {
  cls?: string[];
  As?: keyof JSX.IntrinsicElements | React.ComponentType<any>,
}

const Box = React.forwardRef<any, TBoxProps>((props, ref) => {
  const {
    cls,
    children,
    As = 'div',
    ...other
  } = props

  return (
    <As
      className={classNames(cls)}
      {...other}
    >
      {children}
    </As>
  )
})

Box.displayName = 'Box';

export { Box }
