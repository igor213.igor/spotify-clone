import React from 'react'


import { Box, TBoxProps } from '@components/uiKit/Box'

import { useWindowScrollTop } from '@components/utils/hooks/useWindowScrollTop'

export type TPageLayoutProps = Partial<TBoxProps>;

export const PAGE_MAX_WIDTH = '1366px'

const PageLayout = (props: TPageLayoutProps): React.ReactElement | null => {
  const {
    children,
    ...other
  } = props

  useWindowScrollTop()

  return (
    <Box
      {...other}
    >
      {
        children
      }
    </Box>
  )
}

export {
  PageLayout,
}
