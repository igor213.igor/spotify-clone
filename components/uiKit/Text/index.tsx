import classNames from 'classnames'
import React from 'react'

export type TTextProps = React.ButtonHTMLAttributes<HTMLSpanElement | HTMLParagraphElement> & {
  cls?: string[];
  As?: keyof JSX.IntrinsicElements | React.ComponentType<any>,
}

const Text = (props: TTextProps): React.ReactElement | null => {
  const {
    cls,
    children,
    As = 'span',
    ...other
  } = props

  return (
    <As
      className={classNames(cls)}
      {...other}
    >
      {children}
    </As>
  )
}

export { Text }
