import React from 'react'

import { Box } from '@components/uiKit/Box'


type TInputProps = React.InputHTMLAttributes<HTMLInputElement> & {
  StartIcon?: React.FunctionComponent;
  startIcon?: React.ReactNode;
  EndIcon?: React.FunctionComponent,
  endIcon?: React.ReactNode;
  cssBtn?: string[] | string;
  cssTxt?: string[] | string;
}

const Input = (props: TInputProps): React.ReactElement | null => {
  const {
    startIcon,
    endIcon,
    StartIcon,
    EndIcon,
    cssBtn = '',
    cssTxt,
    ...other
  } = props

  const renderStartIcon = () => {
    if (startIcon) {
      return startIcon
    }

    if (StartIcon) {
      return <StartIcon />
    }

    return null
  }

  const renderEndIcon = () => {
    if (endIcon) {
      return endIcon
    }

    if (EndIcon) {
      return <EndIcon />
    }

    return null
  }

  return (
    <Box>
      {
        renderStartIcon()
      }
      <input
        {...other}
      />
      {
        renderEndIcon()
      }
    </Box>

  )
}

export type {
  TInputProps,
}

export { Input }
