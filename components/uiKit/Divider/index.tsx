import React from 'react'

type TDividerProps = React.ButtonHTMLAttributes<HTMLHRElement>

const Divider = (props: TDividerProps): React.ReactElement | null => {
  const {
    ...other
  } = props

  return (
    <hr
      {...other}
    />
  )
}

export { Divider }
