import { UserIcon } from '@heroicons/react/outline'
import classNames from 'classnames'
import React from 'react'

import { Box } from '@components/uiKit/Box'

type TUserAvatarProps = React.ButtonHTMLAttributes<HTMLDivElement> & {
  img?: string;
  name: string;
}

const UserAvatar = (props: TUserAvatarProps): React.ReactElement | null => {
  const {
    img,
    name,
    ...other
  } = props


  const clsName = [
    'font-bold',
    'text-xs',
    'mr-2',
  ]

  const clsImg = [
    'rounded-full',
    'bg-gray-700',
    'h-7',
    'w-7',
    'flex',
    'items-center',
    'justify-center',
  ]

  const cls = [
    'flex',
    'items-center',
    'h-8',
    'bg-neutral-800',
    'rounded-full',
    'text-white',
    'gap-2',
    'p-2px',
  ]


  const renderImg = () => {
    return (
      <div
        className={classNames(clsImg)}>
        <UserIcon
          className='h-4 w-4' />
      </div>
    )
  }

  const renderName = () => {
    return (
      <span
        className={classNames(clsName)}>
        {name || ''}
      </span>
    )
  }

  return (
    <Box
      cls={cls}
      {...other}>

      {
        renderImg()
      }
      {
        renderName()
      }

    </Box>
  )
}

export { UserAvatar }
