import NextImage, { ImageProps } from 'next/image'
import React from 'react'


export type TImageProps = ImageProps;

const Image = (props: TImageProps): React.ReactElement => {
  const {
    src,
    alt,
    ...other
  } = props

  return (
    <NextImage
      src={src}
      alt={alt}
      layout='fill'
      objectFit='contain'
      {...other}
    />
  )
}

export {
  Image,
}
