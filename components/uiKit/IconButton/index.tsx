import classNames from 'classnames'
import React from 'react'


type TIconButtonProps = React.ButtonHTMLAttributes<HTMLButtonElement> & {
  children?: React.ReactNode;
  cssBtn?: string[] | string;
  isDisabled?: boolean;
  onClick: () => void;
}

const IconButton = (props: TIconButtonProps): React.ReactElement | null => {
  const {
    children,
    isDisabled,
    type = 'button',
    cssBtn = '',
    onClick,
    ...other
  } = props


  return (
    <button
      className={classNames(
        [
          'flex',
          'items-center',
          'cursor-pointer',
          'hover:scale-125',
          'transition',
          'transform',
          'duration-100',
          'ease-out',
        ],
        cssBtn
      )}
      onClick={onClick}
      type={type}
      disabled={isDisabled}
      {...other}
    >
      {
        children
      }
    </button>
  )
}

export type {
  TIconButtonProps,
}

export { IconButton }
