import classNames from 'classnames'
import React from 'react'


type TButtonProps = React.ButtonHTMLAttributes<HTMLButtonElement> & {
  children?: React.ReactNode;
  StartIcon?: React.FunctionComponent;
  startIcon?: React.ReactNode;
  EndIcon?: React.FunctionComponent,
  endIcon?: React.ReactNode;
  cssBtn?: string[] | string;
  cssTxt?: string[] | string;
  isActive?: boolean;
  isDisabled?: boolean;
  onClick: () => void;
}

const Button = (props: TButtonProps): React.ReactElement | null => {
  const {
    children,
    startIcon,
    endIcon,
    StartIcon,
    EndIcon,
    type = 'button',
    cssBtn = '',
    cssTxt,
    isDisabled,
    onClick,
  } = props

  const renderStartIcon = () => {
    if (startIcon) {
      return startIcon
    }

    if (StartIcon) {
      return <StartIcon />
    }

    return null
  }

  const renderEndIcon = () => {
    if (endIcon) {
      return endIcon
    }

    if (EndIcon) {
      return <EndIcon />
    }

    return null
  }

  const renderText = () => {
    if (!children) {
      return
    }

    return (
      <span
        className={classNames(cssTxt)}>
       {children}
     </span>
    )
  }

  return (
    <button
      className={classNames(
        [
          'flex',
          'items-center',
          'space-x-2',
          'cursor-pointer',
          'hover:scale-125',
          'transition',
          'transform',
          'duration-100',
          'ease-out',
        ],
        cssBtn
      )}
      onClick={onClick}
      type={type}
      disabled={!!isDisabled}
    >
      {
        renderStartIcon()
      }
      {
        renderText()
      }
      {
        renderEndIcon()
      }
    </button>
  )
}

export type {
  TButtonProps,
}

export { Button }
