enum ENVIRONMENT {
  PRODUCTION = 'production',
  DEVELOPMENT = 'development',
  LOCAL = 'local'
}

enum TRANS {
  MAIN = 'main',
}

enum LOCALE {
  EN = 'en',
  RU = 'ru',
}

enum AUTH_STATUS {
  AUTHENTICATED = 'authenticated',
  LOADING = 'loading',
  UNAUTHENTICATED = 'unauthenticated'
}

enum FETCH_STATUS {
  LOADING = 'loading',
  SUCCEEDED = 'succeeded',
  FAILED = 'failed',
  INITIAL='initial'
}

type TEntityID = string | number;

type TFetchStatus = {
  status: FETCH_STATUS
}


export {
  AUTH_STATUS,
  ENVIRONMENT,
  TRANS,
  LOCALE,
  FETCH_STATUS,
}
export type {
  TEntityID,
  TFetchStatus,
}

