import { signIn, useSession } from 'next-auth/react'
import { useEffect } from 'react'

import { TAppSession } from '@lib/nextAuthHelper'
import { spotifyLib } from '@lib/spotify'

const useSpotify = () => {
  const { data } = useSession()

  const currSession = data as TAppSession

  useEffect(() => {
    if (!currSession || !currSession.accessToken) {
      return
    }
    spotifyLib.setAccessToken(currSession.accessToken)
  }, [currSession])


  return spotifyLib
}

export {
  useSpotify,
}
