import { useEffect } from 'react';

import { isServer } from '@lib/common'

const useWindowScrollTop = () => {
  useEffect(() => {
    if (isServer()) {
      return;
    }

    window.scrollTo({
      top: 0,
      left: 0,
    });
  }, []);
};

export {
  useWindowScrollTop,
};
