import Link from 'next/link'
import React from 'react'

import { Image, TImageProps } from '@components/uiKit/Image'

import { routing } from '@lib/routing'

type TLogoProps = Partial<TImageProps>;

const Logo = (props: TLogoProps): React.ReactElement | null => {
  const { ...other } = props

  return (
    <Link
      href={routing.getIndexUrl()}
    >
      <a>
        <Image
          src='/images/spotify-logo.png'
          alt='Logo'
          {...other}
        />
      </a>
    </Link>
  )
}

export {
  Logo,
}
