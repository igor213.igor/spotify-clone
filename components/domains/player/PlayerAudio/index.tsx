import React, { useEffect, useRef } from 'react'
import { useSelector } from 'react-redux'

import { playerActions } from '@Redux/domains/player/reducer'
import { playerSelectors } from '@Redux/domains/player/selectors'
import { useAppDispatch } from '@Redux/hooks'

type TPlayerAudioProps = {}

const PlayerAudio = (props: TPlayerAudioProps): React.ReactElement | null => {
  const {
    ...other
  } = props

  const audioRef = useRef<HTMLAudioElement>(null)

  const previewTrack = useSelector(playerSelectors.getAudioPreviewFromTrack)
  const volume = useSelector(playerSelectors.getVolume)
  const hasPlay = useSelector(playerSelectors.getHasPlay)

  const dispatch = useAppDispatch()

  const handlePlayOff = () => {
    dispatch(playerActions.setPlay(false))
  }


  useEffect(() => {
    if (audioRef.current) {
      audioRef.current.volume = volume
    }
  }, [previewTrack, volume])

  useEffect(() => {
    if (audioRef.current) {
      hasPlay ? audioRef.current.play() : audioRef.current.pause()
    }
  }, [previewTrack, hasPlay])


  useEffect(() => {
    if (!audioRef.current) {
      return
    }

    audioRef.current.addEventListener('ended', () => {
      handlePlayOff()
    })

    return () => {
      if (!audioRef.current) {
        return
      }
      audioRef.current.removeEventListener('ended', () => handlePlayOff())
    }
  }, [])


  return (
    <audio
      src={previewTrack}
      ref={audioRef}
    />
  )
}

export { PlayerAudio }
