import React from 'react'
import { useSelector } from 'react-redux'

import { playerSelectors } from '@Redux/domains/player/selectors'

import { Box, TBoxProps } from '@components/uiKit/Box'
import { Image } from '@components/uiKit/Image'
import { Text } from '@components/uiKit/Text'

type TPlayerMetaInfoProps = TBoxProps & {}

const PlayerMetaInfo = (props: TPlayerMetaInfoProps): React.ReactElement | null => {
  const {
    ...other
  } = props

  const cls = [
    'flex',
    'gap-3',
  ]

  const trackImages = useSelector(playerSelectors.getAlbumImagesFromTrack)
  const trackName = useSelector(playerSelectors.getNameFromTrack)
  const trackArtists = useSelector(playerSelectors.getArtistsFromTrack)

  const renderImages = () => {
    if (!trackImages.length) {
      return null
    }

    return (
      <Image
        src={trackImages[1]?.url || ''}
        alt={'track logo'}
      />
    )
  }

  return (
    <Box
      cls={cls}
      {...other}
    >
      <Box
        cls={['hidden', 'md:block', 'relative', 'h-10', 'w-10']}>
        {
          renderImages()
        }
      </Box>
      <Box
        cls={['flex', 'flex-col']}>
        <Text
        cls={['text-sm']}>
          {trackName}
        </Text>
        <Text
          cls={['text-gray-500','text-xs']}>
          {trackArtists.map(art => art.name).join(', ')}
        </Text>
      </Box>


    </Box>
  )
}

export { PlayerMetaInfo }
