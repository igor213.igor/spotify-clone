import React from 'react'
import { useSelector } from 'react-redux'

import { playerSelectors } from '@Redux/domains/player/selectors'

import { PlayerAudio } from '@components/domains/player/PlayerAudio'
import { PlayerControl } from '@components/domains/player/PlayerControl'
import { PlayerMetaInfo } from '@components/domains/player/PlayerMetaInfo'
import { PlayerVolume } from '@components/domains/player/PlayerVolume'

import { Box, TBoxProps } from '@components/uiKit/Box'

import { FETCH_STATUS } from '@components/utils/types'

type TPlayerProps = TBoxProps & {}

const Player = (props: TPlayerProps): React.ReactElement | null => {
  const {
    ...other
  } = props

  const cls = [
    'grid',
    'grid-cols-3',
    'p-4',
    'bg-gradient-to-b',
    'from-black',
    'to-gray-900',
  ]

  return (
    <Box
      cls={cls}
      {...other}>
      <PlayerMetaInfo />
      <PlayerControl />
      <PlayerVolume />
      <PlayerAudio />
    </Box>
  )
}

export { Player }
