import { PauseIcon } from '@heroicons/react/solid'
import React from 'react'

import { playerActions } from '@Redux/domains/player/reducer'
import { useAppDispatch } from '@Redux/hooks'

import { IconButton, TIconButtonProps } from '@components/uiKit/IconButton'

type TPlayerControlPauseBtnProps = Partial<TIconButtonProps>

const PlayerControlPauseBtn = (props: TPlayerControlPauseBtnProps): React.ReactElement | null => {
  const {
    ...other
  } = props


  const dispatch = useAppDispatch()

  const handleClick = () => {
    dispatch(playerActions.setPlay(false))
  }


  return (
    <IconButton
      onClick={handleClick}
      {...other}
    >
      <PauseIcon
        className='h-10 w-10'
      />
    </IconButton>
  )
}

export { PlayerControlPauseBtn }
