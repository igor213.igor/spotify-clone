import { ReplyIcon } from '@heroicons/react/solid'
import React from 'react'

import { IconButton, TIconButtonProps } from '@components/uiKit/IconButton'

type TPlayerControlReplyBtnProps = Partial<TIconButtonProps>

const PlayerControlReplyBtn = (props: TPlayerControlReplyBtnProps): React.ReactElement | null => {
  const {
    ...other
  } = props

  const cls = []

  const handleClick = () => {

  }

  return (
    <IconButton
      onClick={handleClick}
      {...other}
    >
      <ReplyIcon
        className='h-5 w-5'
      />
    </IconButton>
  )
}

export { PlayerControlReplyBtn }
