import { VolumeOffIcon } from '@heroicons/react/solid'
import React from 'react'

import { IconButton, TIconButtonProps } from '@components/uiKit/IconButton'

type TPlayerVolumeDownBtnProps = Partial<TIconButtonProps>

const PlayerVolumeDownBtn = (props: TPlayerVolumeDownBtnProps): React.ReactElement | null => {
  const {
    ...other
  } = props

  const handleClick = () => {

  }

  return (
    <IconButton
      onClick={handleClick}
      {...other}
    >
      <VolumeOffIcon
        className='h-5 w-5'
      />
    </IconButton>
  )
}

export { PlayerVolumeDownBtn }
