import { VolumeUpIcon } from '@heroicons/react/solid'
import React from 'react'

import { IconButton, TIconButtonProps } from '@components/uiKit/IconButton'

type TPlayerVolumeUpBtnProps = Partial<TIconButtonProps>

const PlayerVolumeUpBtn = (props: TPlayerVolumeUpBtnProps): React.ReactElement | null => {
  const {
    ...other
  } = props

  const cls = []

  const handleClick = () => {

  }

  return (
    <IconButton
      onClick={handleClick}
      {...other}
    >
      <VolumeUpIcon
        className='h-5 w-5'
      />
    </IconButton>
  )
}

export { PlayerVolumeUpBtn }
