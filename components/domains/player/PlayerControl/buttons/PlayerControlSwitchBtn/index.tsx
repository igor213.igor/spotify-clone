import { SwitchHorizontalIcon } from '@heroicons/react/outline'
import React from 'react'

import { IconButton, TIconButtonProps } from '@components/uiKit/IconButton'

type TPlayerControlSwitchBtnProps = Partial<TIconButtonProps>

const PlayerControlSwitchBtn = (props: TPlayerControlSwitchBtnProps): React.ReactElement | null => {
  const {
    ...other
  } = props

  const cls = []

  const handleClick = () => {

  }

  return (
    <IconButton
      onClick={handleClick}
      {...other}
    >
      <SwitchHorizontalIcon
        className='h-5 w-5'
      />
    </IconButton>
  )
}

export { PlayerControlSwitchBtn }
