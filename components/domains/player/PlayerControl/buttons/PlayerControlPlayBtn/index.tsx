import { PlayIcon } from '@heroicons/react/solid'
import React from 'react'
import { useSelector } from 'react-redux'

import { playerActions } from '@Redux/domains/player/reducer'
import { playerSelectors } from '@Redux/domains/player/selectors'
import { useAppDispatch } from '@Redux/hooks'

import { IconButton, TIconButtonProps } from '@components/uiKit/IconButton'

type TPlayerControlPlayBtnProps = Partial<TIconButtonProps>

const PlayerControlPlayBtn = (props: TPlayerControlPlayBtnProps): React.ReactElement | null => {
  const {
    ...other
  } = props

  const dispatch = useAppDispatch()

  const currIdTrack = useSelector(playerSelectors.getCurrId)

  const handleClick = () => {
    if (!currIdTrack) {
      return
    }

    dispatch(playerActions.setPlay(true))
  }

  return (
    <IconButton
      onClick={handleClick}
      {...other}
    >
      <PlayIcon
        className='h-10 w-10'
      />
    </IconButton>
  )
}

export { PlayerControlPlayBtn }
