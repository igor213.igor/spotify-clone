import { RewindIcon } from '@heroicons/react/solid'
import React from 'react'

import { IconButton, TIconButtonProps } from '@components/uiKit/IconButton'

type TPlayerControlRewindBtnProps = Partial<TIconButtonProps>

const PlayerControlRewindBtn = (props: TPlayerControlRewindBtnProps): React.ReactElement | null => {
  const {
    ...other
  } = props

  const cls = []

  const handleClick = () => {

  }

  return (
    <IconButton
      onClick={handleClick}
      {...other}
    >
      <RewindIcon
        className='h-5 w-5'
      />
    </IconButton>
  )
}

export { PlayerControlRewindBtn }
