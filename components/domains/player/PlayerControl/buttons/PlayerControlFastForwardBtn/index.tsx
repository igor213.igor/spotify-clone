import { FastForwardIcon } from '@heroicons/react/solid'
import React from 'react'

import { IconButton, TIconButtonProps } from '@components/uiKit/IconButton'

type TPlayerControlFastForwardBtnProps = Partial<TIconButtonProps>

const PlayerControlFastForwardBtn = (props: TPlayerControlFastForwardBtnProps): React.ReactElement | null => {
  const {
    ...other
  } = props

  const cls = []

  const handleClick = () => {

  }

  return (
    <IconButton
      onClick={handleClick}
      {...other}
    >
      <FastForwardIcon
        className='h-5 w-5'
      />
    </IconButton>
  )
}

export { PlayerControlFastForwardBtn }
