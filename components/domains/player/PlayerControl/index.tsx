import React from 'react'
import { useSelector } from 'react-redux'

import { playerSelectors } from '@Redux/domains/player/selectors'

import {
  PlayerControlFastForwardBtn,
} from '@components/domains/player/PlayerControl/buttons/PlayerControlFastForwardBtn'
import { PlayerControlPauseBtn } from '@components/domains/player/PlayerControl/buttons/PlayerControlPauseBtn'
import { PlayerControlPlayBtn } from '@components/domains/player/PlayerControl/buttons/PlayerControlPlayBtn'
import { PlayerControlReplyBtn } from '@components/domains/player/PlayerControl/buttons/PlayerControlReplyBtn'
import { PlayerControlRewindBtn } from '@components/domains/player/PlayerControl/buttons/PlayerControlRewindBtn'
import { PlayerControlSwitchBtn } from '@components/domains/player/PlayerControl/buttons/PlayerControlSwitchBtn'

import { Box, TBoxProps } from '@components/uiKit/Box'


type TPlayerControlProps = TBoxProps & {}

const PlayerControl = (props: TPlayerControlProps): React.ReactElement | null => {
  const {
    ...other
  } = props

  const cls = [
    'flex',
    'items-center',
    'justify-evenly',
  ]

  const currIdTrack = useSelector(playerSelectors.getCurrId)
  const hasPlay = useSelector(playerSelectors.getHasPlay)

  return (
    <Box
      cls={cls}
      {...other}>
      <PlayerControlSwitchBtn />
      <PlayerControlRewindBtn />
      {
        (!currIdTrack || !hasPlay)
          ? <PlayerControlPlayBtn />
          : <PlayerControlPauseBtn />
      }
      <PlayerControlFastForwardBtn />
      <PlayerControlReplyBtn />
    </Box>
  )
}

export { PlayerControl }
