import { debounce } from 'lodash'
import React from 'react'
import { useSelector } from 'react-redux'

import { playerActions } from '@Redux/domains/player/reducer'
import { playerSelectors } from '@Redux/domains/player/selectors'
import { useAppDispatch } from '@Redux/hooks'

import { PlayerVolumeDownBtn } from '@components/domains/player/PlayerControl/buttons/PlayerVolumeDownBtn'
import { PlayerVolumeUpBtn } from '@components/domains/player/PlayerControl/buttons/PlayerVolumeUpBtn'

import { Box, TBoxProps } from '@components/uiKit/Box'
import { Input } from '@components/uiKit/Input'

type TPlayerVolumeProps = TBoxProps & {}

const PlayerVolume = (props: TPlayerVolumeProps): React.ReactElement | null => {
  const {
    ...other
  } = props

  const cls = [
    'flex',
    'items-center',
    'space-x-3',
    'md:space-x-4',
    'justify-end',
  ]

  const dispatch = useAppDispatch()

  const volume = useSelector(playerSelectors.getVolume)


  const handleChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    const value = e.target.value
    //debounce(() => {
    dispatch(playerActions.setVolume(Number(value) / 100))
    //}, 20)
  }

  return (
    <Box
      cls={cls}
      {...other}
    >
      <PlayerVolumeDownBtn />
      <Input
        type='range'
        value={volume * 100}
        min={0}
        max={100}
        onChange={handleChange}
      />
      <PlayerVolumeUpBtn />
    </Box>
  )
}

export { PlayerVolume }
