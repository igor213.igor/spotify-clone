import { useSession } from 'next-auth/react'
import { useRouter } from 'next/router'
import React, { useEffect } from 'react'
import { useSelector } from 'react-redux'

import { asyncPlayListActions } from '@Redux/domains/playlist/asyncActions'
import { asyncPlayListsActions } from '@Redux/domains/playlists/asyncActions'
import { playListsActions } from '@Redux/domains/playlists/reducer'
import { playListsSelectors } from '@Redux/domains/playlists/selectors'
import { useAppDispatch } from '@Redux/hooks'

import { useSpotify } from '@components/utils/hooks/useSpotify'

const PlayListLoadData = (): React.ReactElement | null => {

  const spotify = useSpotify()

  const dispatch = useAppDispatch()

  const { query } = useRouter()

  const { data: session } = useSession({
    required: true,
  })

  const currIdPlaylist = useSelector(playListsSelectors.getCurrId)

  useEffect(() => {
    if (query && query.id) {
      dispatch(playListsActions.setCurrId(query.id.toString()))
    }
  }, [query])

  useEffect(() => {
    if (spotify.getAccessToken()) {
      dispatch(asyncPlayListsActions.getUserPlayLists())
    }
  }, [spotify, session])

  useEffect(() => {
    if (currIdPlaylist && spotify.getAccessToken()) {
      dispatch(asyncPlayListActions.getPlaylist(currIdPlaylist))
    }
  }, [spotify, session, currIdPlaylist])


  return null
}

export {
  PlayListLoadData,
}
