import { DotsHorizontalIcon } from '@heroicons/react/outline'
import React from 'react'

import { Box, TBoxProps } from '@components/uiKit/Box'
import { IconButton } from '@components/uiKit/IconButton'

type TPlayListMoreMenuProps = TBoxProps;

const PlayListMoreMenu = (props: TPlayListMoreMenuProps): React.ReactElement | null => {
  const { ...other } = props

  const handleClick = () => {

  }

  return (
    <Box
      cls={['px-8','py-6']}
      {...other}>
      <IconButton
        onClick={handleClick}
      >
        <DotsHorizontalIcon
          className='h-8 w-8 text-white'
        />
      </IconButton>
    </Box>
  )
}

export {
  PlayListMoreMenu,
}
