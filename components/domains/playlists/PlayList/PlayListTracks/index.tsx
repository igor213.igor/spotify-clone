import React from 'react'
import { useSelector } from 'react-redux'

import { playlistRepoSelectors } from '@Redux/domains/playListRepo/selectors'

import { PlayListTrack } from '@components/domains/playlists/PlayList/PlayListTracks/PlayListTrack'

import { Box, TBoxProps } from '@components/uiKit/Box'

type TPlayListSongsProps = TBoxProps;

const PlayListTracks = (props: TPlayListSongsProps): React.ReactElement | null => {

  const {
    ...other
  } = props

  const hasPlayList = useSelector(playlistRepoSelectors.getHasPlayList)
  const trackIds = useSelector(playlistRepoSelectors.getTracksIds)

  if (!hasPlayList || !trackIds.length) {
    return null
  }

  return (
    <Box
      cls={['px-8', 'space-y-2', 'mb-36']}
      {...other}>
      {
        trackIds.map((id, idx) =>
          <PlayListTrack
            key={id}
            idx={idx}
            trackId={id}
          />
        )
      }
    </Box>
  )
}

export {
  PlayListTracks,
}
