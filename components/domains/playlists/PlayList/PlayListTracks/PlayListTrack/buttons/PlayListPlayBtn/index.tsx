import React from 'react'
import { useSelector } from 'react-redux'

import { asyncPlayerActions } from '@Redux/domains/player/asyncActions'
import { playerActions } from '@Redux/domains/player/reducer'
import { playlistRepoSelectors } from '@Redux/domains/playListRepo/selectors'
import { useAppDispatch } from '@Redux/hooks'

import { Box, TBoxProps } from '@components/uiKit/Box'


type TPlayListPlayBtnProps = TBoxProps & {
  trackId: string;
}

const PlayListPlayBtn = (props: TPlayListPlayBtnProps): React.ReactElement | null => {
  const {
    trackId,
    ...other
  } = props

  const dispatch = useAppDispatch()

  const uri = useSelector(playlistRepoSelectors.getUriFromTrack)(trackId)

  const handleClickTrack = async () => {
    if (typeof uri !== 'string') {
      return
    }

    dispatch(playerActions.setCurrId(trackId))
    await dispatch(asyncPlayerActions.getTrackInfo(trackId))
    dispatch(playerActions.setPlay(true))
  }


  return (
    <Box
      cls={[
        'absolute',
        'h-full',
        'w-full',
      ]}
      onClick={handleClickTrack}
      {...other}
    />
  )
}

export {
  PlayListPlayBtn,
}
