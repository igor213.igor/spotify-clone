import React from 'react'
import { useSelector } from 'react-redux'

import { playlistRepoSelectors } from '@Redux/domains/playListRepo/selectors'

import { Box, TBoxProps } from '@components/uiKit/Box'
import { Text } from '@components/uiKit/Text'

import { dateHelpers } from '@lib/common/date'

type TPlayListDurationProps = TBoxProps & {
  trackId: string;
};

const PlayListDuration = (props: TPlayListDurationProps): React.ReactElement | null => {

  const {
    trackId,
    ...other
  } = props

  const duration = useSelector(playlistRepoSelectors.getDurationFromTrack)(trackId)


  return (
    <Box
      cls={['flex', 'items-center', 'justify-end']}
      {...other}>
      <Text>
        {dateHelpers.getMinutesAndSecondsFromMillis(duration)}
      </Text>
    </Box>
  )
}

export {
  PlayListDuration,
}
