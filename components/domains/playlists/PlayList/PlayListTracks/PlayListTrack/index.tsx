import { isEqual } from 'lodash'
import React from 'react'
import { useSelector } from 'react-redux'

import { playerSelectors } from '@Redux/domains/player/selectors'
import { playlistRepoSelectors } from '@Redux/domains/playListRepo/selectors'

import {
  PlayListPlayBtn,
} from '@components/domains/playlists/PlayList/PlayListTracks/PlayListTrack/buttons/PlayListPlayBtn'
import { PlayListAlbum } from '@components/domains/playlists/PlayList/PlayListTracks/PlayListTrack/PlayListAlbum'
import { PlayListDuration } from '@components/domains/playlists/PlayList/PlayListTracks/PlayListTrack/PlayListDuration'
import { PlayListMetaInfo } from '@components/domains/playlists/PlayList/PlayListTracks/PlayListTrack/PlayListMetaInfo'

import { Box, TBoxProps } from '@components/uiKit/Box'


type TPlayListTrackProps = TBoxProps & {
  trackId: string;
  idx: number
}

const PlayListTrack = (props: TPlayListTrackProps): React.ReactElement | null => {
  const {
    trackId,
    idx,
    ...other
  } = props

  const hasTrack = useSelector(playlistRepoSelectors.hasTrackById)(trackId)

  const currTrackId = useSelector(playerSelectors.getCurrId)

  const isActiveTrack = isEqual(currTrackId, trackId)

  const handleClickTrack = () => {

  }


  if (!hasTrack) {
    return null
  }

  return (
    <Box
      cls={[
        'grid',
        'grid-cols-3',
        'cursor-pointer',
        'text-gray-500',
        'p-2',
        'rounded-md',
        'relative',
        'hover:bg-neutral-700',
        'hover:text-white',
        'tracks',
        isActiveTrack ? 'bg-neutral-700' : '',
      ]}
      onClick={handleClickTrack}
      {...other}
    >
      <PlayListPlayBtn
        trackId={trackId}
      />
      <PlayListMetaInfo
        idx={idx}
        trackId={trackId}
        isActive={isActiveTrack}
      />
      <PlayListAlbum
        trackId={trackId}
      />
      <PlayListDuration
        trackId={trackId}
      />
    </Box>
  )
}

export {
  PlayListTrack,
}
