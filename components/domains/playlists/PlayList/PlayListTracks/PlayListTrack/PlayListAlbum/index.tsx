import React from 'react'
import { useSelector } from 'react-redux'

import { playlistRepoSelectors } from '@Redux/domains/playListRepo/selectors'

import { Box, TBoxProps } from '@components/uiKit/Box'
import { Text } from '@components/uiKit/Text'

type TPlayListAlbumProps = TBoxProps & {
  trackId: string
};

const PlayListAlbum = (props: TPlayListAlbumProps): React.ReactElement | null => {

  const {
    trackId,
    ...other
  } = props

  const albumName = useSelector(playlistRepoSelectors.getAlbumNameFromTrack)(trackId)


  return (
    <Box
      cls={['flex', 'items-center']}
      {...other}>
      <Text>
        {albumName}
      </Text>
    </Box>
  )
}

export {
  PlayListAlbum,
}
