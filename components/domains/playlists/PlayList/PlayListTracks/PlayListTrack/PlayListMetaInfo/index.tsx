import { PlayIcon } from '@heroicons/react/outline'
import React from 'react'
import { useSelector } from 'react-redux'

import { playerSelectors } from '@Redux/domains/player/selectors'
import { playlistRepoSelectors } from '@Redux/domains/playListRepo/selectors'

import { Box, TBoxProps } from '@components/uiKit/Box'
import { IconButton } from '@components/uiKit/IconButton'
import { Image } from '@components/uiKit/Image'
import { Text } from '@components/uiKit/Text'

type TPlayListMetaInfoProps = TBoxProps & {
  idx: number;
  trackId: string;
  isActive: boolean;
};

const playTrackImg = 'https://open.scdn.co/cdn/images/equaliser-animated-green.f93a2ef4.gif'

const PlayListMetaInfo = (props: TPlayListMetaInfoProps): React.ReactElement | null => {

  const {
    idx,
    trackId,
    isActive,
    ...other
  } = props

  const artists = useSelector(playlistRepoSelectors.getArtistsFromTrack)(trackId)
  const trackName = useSelector(playlistRepoSelectors.getNameFromTrack)(trackId)
  const albumImages = useSelector(playlistRepoSelectors.getAlbumImagesFromTrack)(trackId)
  const hasPlay = useSelector(playerSelectors.getHasPlay)

  const renderOrder = () => {
    if (isActive && hasPlay) {
      return (
        <Box
          cls={['relative', 'h-5', 'w-5']}>
          <Image
            src={playTrackImg}
            alt={'play track'}
          />
        </Box>
      )
    }

    return (
      <Box
        cls={['h-5', 'w-5']}>
        <Text
          cls={['track-order',isActive ? 'text-green-600' : '']}>
          {idx + 1}
        </Text>
        <PlayIcon
          className='track-play--btn'
        />
      </Box>
    )
  }

  const renderOrderAndImgAlbum = () => {
    return (
      <Box
        cls={['flex', 'items-center', 'space-x-4']}>
        {
          renderOrder()
        }
        <Box
          cls={['h-10', 'w-10', 'relative']}>
          <Image
            src={albumImages[0]?.url || ''}
            alt='Album logo'
          />
        </Box>
      </Box>

    )
  }

  const renderNameAndArtists = () => {
    const artistsName = artists.map(artist => artist.name)

    return (
      <Box
        cls={['flex', 'flex-col']}>
        <Text
          cls={[
            'text-white',
            isActive ? 'text-green-600' : '',
          ]}
        >
          {trackName}
        </Text>
        <Text>
          {artistsName.join(', ')}
        </Text>
      </Box>
    )
  }

  return (
    <Box
      cls={['flex', 'items-center', 'space-x-4']}
      {...other}>
      {
        renderOrderAndImgAlbum()
      }
      {
        renderNameAndArtists()
      }
    </Box>
  )
}

export {
  PlayListMetaInfo,
}
