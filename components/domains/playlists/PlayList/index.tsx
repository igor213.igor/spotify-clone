import React from 'react'

import { PlayListHeader } from '@components/domains/playlists/PlayList/PlayListHeader'
import { PlayListMoreMenu } from '@components/domains/playlists/PlayList/PlayListMoreMenu'
import { PlayListTracks } from '@components/domains/playlists/PlayList/PlayListTracks'

import { TBoxProps } from '@components/uiKit/Box'

type TPlayListProps = TBoxProps

const PlayList = (props: TPlayListProps): React.ReactElement | null => {
  const { ...other } = props

  return (
    <>
      <PlayListHeader />
      <PlayListMoreMenu />
      <PlayListTracks />
    </>
  )
}

export {
  PlayList,
}
