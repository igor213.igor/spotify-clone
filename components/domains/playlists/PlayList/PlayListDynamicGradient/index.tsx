import { shuffle } from 'lodash'
import React from 'react'

import { Box, TBoxProps } from '@components/uiKit/Box'

import { isServer } from '@lib/common'

type TPlayListDynamicGradientProps = TBoxProps;


const PlayListDynamicGradient = (props: TPlayListDynamicGradientProps): React.ReactElement | null => {
  const {
    ...other
  } = props

  const bgGradientList = [
    'from-indigo-500',
    'from-blue-500',
    'from-green-500',
    'from-red-500',
    'from-yellow-500',
    'from-pink-500',
    'from-purple-500',
  ]

  const cls = [
    'absolute',
    'h-full',
    'w-full',
    'bg-gradient-to-b',
    'to-black',
    shuffle(bgGradientList).pop() as string,
  ]


  return (
    <Box
      cls={cls}
      {...other}
    />
  )
}

export {
  PlayListDynamicGradient,
}
