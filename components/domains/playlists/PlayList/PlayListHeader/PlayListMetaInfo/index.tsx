import { useTranslation } from 'next-i18next'
import React from 'react'
import { useSelector } from 'react-redux'

import { playListsSelectors } from '@Redux/domains/playlists/selectors'

import { Box } from '@components/uiKit/Box'
import { Text } from '@components/uiKit/Text'

import { TRANS } from '@components/utils/types'

const PlayListMetaInfo = (): React.ReactElement | null => {

  const { t } = useTranslation(TRANS.MAIN)

  const playlistOwner = useSelector(playListsSelectors.getOwnerFromCurrId)
  const playlistName = useSelector(playListsSelectors.getNameFromCurrId)

  return (
    <Box
      cls={[]}>

      <Text
        cls={['text-xs','uppercase','font-bold']}
        As='h2'>
        {t('pages.playList.metaInfo.title')}
      </Text>

      <Text
        cls={['text-4xl','my-2','font-bold','lg:text-5xl','xl:text-7xl']}
        As='h1'>
        {playlistName}
      </Text>

      <Text
        cls={['font-bold','text-base']}
       >
        {playlistOwner?.display_name}
      </Text>

    </Box>
  )
}

export {
  PlayListMetaInfo,
}
