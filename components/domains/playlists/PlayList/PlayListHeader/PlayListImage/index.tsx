import { MusicNoteIcon } from '@heroicons/react/outline'
import React from 'react'
import { useSelector } from 'react-redux'

import { playListsSelectors } from '@Redux/domains/playlists/selectors'

import { Box } from '@components/uiKit/Box'
import { Image } from '@components/uiKit/Image'

const WIDTH_IMG = 232
const HEIGHT_IMG = 232

const PlayListImage = (): React.ReactElement | null => {

  const playlistImg = useSelector(playListsSelectors.getImagesFromCurrId)


  const renderImage = () => {

    const playlistUrlImg = playlistImg[0]?.url

    if (!playlistUrlImg) {
      return (
        <MusicNoteIcon
          className='h-12 text-stone-400'
        />
      )
    }

    return (
      <Image
        src={playlistUrlImg}
        objectFit='cover'
        objectPosition='center center'
      />
    )
  }


  return (
    <Box
      cls={['flex', 'relative', 'mr-6', 'bg-neutral-800', 'items-center', 'justify-center', 'bx-shadow-playlist-img']}
      style={{ width: WIDTH_IMG + 'px', height: HEIGHT_IMG + 'px' }}
    >
      {
        renderImage()
      }
    </Box>
  )
}

export {
  PlayListImage,
}
