import React from 'react'

import { PlayListDynamicGradient } from '@components/domains/playlists/PlayList/PlayListDynamicGradient'
import { PlayListImage } from '@components/domains/playlists/PlayList/PlayListHeader/PlayListImage'
import { PlayListMetaInfo } from '@components/domains/playlists/PlayList/PlayListHeader/PlayListMetaInfo'

import { Box } from '@components/uiKit/Box'

const MAX_HEIGHT_HEADER = 500
const MIN_HEIGHT_HEADER = 340

const PlayListHeader = (): React.ReactElement | null => {

  const cls = ['-mt-64px', 'relative']

  return (
    <Box
      cls={cls}
    >
      <PlayListDynamicGradient />
      <Box
        cls={['flex', 'items-end', 'pb-6']}
        style={{
          height: 30 + 'vh',
          maxHeight: MAX_HEIGHT_HEADER + 'px',
          minHeight: MIN_HEIGHT_HEADER + 'px',
        }}>
        <Box
          cls={['flex', 'items-end', 'px-8','z-2']}>
          <PlayListImage />
          <PlayListMetaInfo />
        </Box>
      </Box>
    </Box>
  )
}

export {
  PlayListHeader,
}
