import React from 'react'

import { Footer } from '@components/domains/layouts/Footer'
import { Header } from '@components/domains/layouts/Header'
import { Sidebar } from '@components/domains/layouts/Sidebar'
import { WIDTH_SIDEBAR } from '@components/domains/layouts/Sidebar/SidebarWrapper'

import { Box, TBoxProps } from '@components/uiKit/Box'

type TMainLayoutProps = TBoxProps;

const MainLayout = (props: TMainLayoutProps): React.ReactElement | null => {
  const { children } = props

  const clsSidebar = [
    'bg-black',
    'home-sidebar-template',
    'text-gray-500',
    'p-5',
    'text-sm',
    'border-r',
    'border-gray-900',
    'pt-6',
    'fixed',
    'top-0',
    'bottom-0',
    'left-0',
    'w-60',
  ]

  return (
    <Box
      cls={['main', 'bg-black', 'h-screen', 'overflow-y-scroll']}>
      <Sidebar
        cls={clsSidebar}
      />
      <Box
        cls={['ml-60']}
      >
        <Header />
        {
          children
        }
      </Box>
      <Footer />
    </Box>
  )
}

export {
  MainLayout,
}
