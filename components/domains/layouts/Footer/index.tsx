import React from 'react'

import { Player } from '@components/domains/player'

import { Box, TBoxProps } from '@components/uiKit/Box'

type TFooterProps = TBoxProps & {}

const Footer = (props: TFooterProps): React.ReactElement | null => {
  const {
    ...other
  } = props

  const cls = [
    'fixed',
    'bottom-0',
    'left-0',
    'right-0',
    'text-white',
    'z-4',
    'bg-neutral-800',
  ]

  return (
    <Box
      cls={cls}
      {...other}>
      <Player />
    </Box>
  )
}

export { Footer }
