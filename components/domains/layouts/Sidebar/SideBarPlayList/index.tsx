import React from 'react'
import { useSelector } from 'react-redux'

import { playListsSelectors } from '@Redux/domains/playlists/selectors'

import { SideBarPlaylistItem } from '@components/domains/layouts/Sidebar/SideBarPlayList/SideBarPlayListitem'

import { Box, TBoxProps } from '@components/uiKit/Box'

type TSideBarPlaylistProps = Partial<TBoxProps>;

const SideBarPlaylist = (props: TSideBarPlaylistProps): React.ReactElement | null => {
  const {
    ...other
  } = props

  const playListsIds = useSelector(playListsSelectors.getIds)

  if (!playListsIds.length) {
    return null
  }

  return (
    <Box
      cls={['space-y-3']}
      {...other}
    >
      {playListsIds.map((id) =>
        <SideBarPlaylistItem
          key={id.toString()}
          id={id.toString()}
        />
      )}
    </Box>
  )
}

export { SideBarPlaylist }
