import Link from 'next/link'
import React from 'react'
import { useSelector } from 'react-redux'

import { playListsSelectors } from '@Redux/domains/playlists/selectors'

import { SideBarItem } from '@components/domains/layouts/Sidebar/SideBaritem'

import { Text } from '@components/uiKit/Text'

import { routing } from '@lib/routing'

type TSideBarPlaylistItemProps = {
  id: string;
};

const SideBarPlaylistItem = (props: TSideBarPlaylistItemProps): React.ReactElement | null => {
  const {
    id,
  } = props

  const playList = useSelector(playListsSelectors.getById)(id)

  if (!playList) {
    return null
  }

  return (
    <SideBarItem
      cls={['font-normal']}
      currPathName={routing.getPlaylistUrl(id)}>
      {
        isActiveRoute =>
          <Link
            href={routing.getPlaylistUrl(id)}
          >
            <a>
              <Text
                cls={['sidebar-item-txt']}>
                {playList.name}
              </Text>

            </a>
          </Link>
      }
    </SideBarItem>
  )
}

export { SideBarPlaylistItem }
