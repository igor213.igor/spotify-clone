import React from 'react'

import { Box } from '@components/uiKit/Box'

type TSidebarContentProps = {
  children: React.ReactNode;
}

const SidebarContent = (props: TSidebarContentProps): React.ReactElement | null => {
  const { children } = props

  const cls = [
    'space-y-4',
  ]

  return (
    <Box
      cls={cls}>
      {
        children
      }
    </Box>
  )
}

export { SidebarContent }
