import React from 'react'

import { SidebarContent } from '@components/domains/layouts/Sidebar/SidebarContent'
import { SideBarCreatePlayList } from '@components/domains/layouts/Sidebar/SideBarCreatePlayList'
import { SidebarHome } from '@components/domains/layouts/Sidebar/SideBarHome'
import { SidebarLibrary } from '@components/domains/layouts/Sidebar/SideBarLibrary'
import { SidebarLogo } from '@components/domains/layouts/Sidebar/SideBarLogo'
import { SideBarPlaylist } from '@components/domains/layouts/Sidebar/SideBarPlayList'
import { SidebarSearch } from '@components/domains/layouts/Sidebar/SideBarSearch'
import { SideBarSongs } from '@components/domains/layouts/Sidebar/SideBarSongs'
import { SidebarWrapper } from '@components/domains/layouts/Sidebar/SidebarWrapper'

import { TBoxProps } from '@components/uiKit/Box'
import { Divider } from '@components/uiKit/Divider'

type TSidebarProps = TBoxProps & {}

const Sidebar = (props: TSidebarProps): React.ReactElement | null => {

  const {
    ...other
  } = props

  return (
    <SidebarWrapper
      {...other}>
      <SidebarLogo />
      <SidebarContent>
        <SidebarHome />
        <SidebarSearch />
        <SidebarLibrary />
        <Divider
          className='border-t-[0.1px] border-transparent'
        />
        <SideBarCreatePlayList />
        <SideBarSongs />
        <Divider
          className='border-t-[0.1px] border-gray-700'
        />
        <SideBarPlaylist />
      </SidebarContent>
    </SidebarWrapper>
  )
}

export { Sidebar }
