import { useRouter } from 'next/router'
import React from 'react'

import { Box, TBoxProps } from '@components/uiKit/Box'

import { routing } from '@lib/routing'
import { routingHelpers } from '@lib/routingHelpers'

type TSideBarItemProps = Partial<TBoxProps> & {
  currPathName: string;
  children: (isActiveRoute: boolean) => React.ReactElement;
};

const SideBarItem = (props: TSideBarItemProps): React.ReactElement | null => {
  const {
    children,
    currPathName,
    cls = [],
    ...other
  } = props

  const { pathname,asPath} = useRouter()

  const isActive = routingHelpers.match(
    pathname,
    currPathName,
    true
  ) ||
    routingHelpers.match(
    asPath,
    currPathName,
    true
  )

  let clsLocal = ['duration-500', 'hover:text-white','sidebar-item']

  if (isActive) {
    clsLocal.push('text-white')
  }

  return (
    <Box
      cls={clsLocal.concat(cls)}
      {...other}>
      {
        children(isActive)
      }
    </Box>
  )
}

export { SideBarItem }
