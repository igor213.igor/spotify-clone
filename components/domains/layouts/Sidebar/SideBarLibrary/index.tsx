import { LibraryIcon } from '@heroicons/react/outline'
import LibraryIconSolid from '@heroicons/react/solid/LibraryIcon'
import { useTranslation } from 'next-i18next'
import Link from 'next/link'
import React from 'react'

import { SideBarItem } from '@components/domains/layouts/Sidebar/SideBaritem'

import { TButtonProps } from '@components/uiKit/Button'
import { Text } from '@components/uiKit/Text'

import { TRANS } from '@components/utils/types'

import { routing } from '@lib/routing'

type TSidebarLibraryProps = Partial<TButtonProps>;

const SidebarLibrary = (props: TSidebarLibraryProps): React.ReactElement | null => {
  const {
    ...other
  } = props

  const { t } = useTranslation(TRANS.MAIN)

  const renderIcon = (isActiveRoute: boolean) => {
    if (!isActiveRoute) {
      return (
        <LibraryIcon
          className='h-6 w-6'
        />
      )
    }

    return (
      <LibraryIconSolid
        className='h-6 w-6'
      />
    )
  }

  return (
    <SideBarItem
      currPathName={routing.getLibraryUrl()}>
      {
        isActiveRoute =>
          <Link
            href={routing.getSearchUrl()}
          >
            <a
              className={['flex', 'items-center', 'space-x-2'].join(' ')}>
              {
                renderIcon(isActiveRoute)
              }
              <Text
                cls={['sidebar-item-txt','font-bold']}>
                {t('sidebar.libraryItem')}
              </Text>
            </a>
          </Link>
      }
    </SideBarItem>
  )
}

export { SidebarLibrary }
