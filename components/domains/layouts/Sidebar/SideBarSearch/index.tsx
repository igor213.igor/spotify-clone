import { SearchIcon } from '@heroicons/react/outline'
import SearchIconSolid from '@heroicons/react/solid/SearchIcon'
import { useTranslation } from 'next-i18next'
import Link from 'next/link'
import React from 'react'

import { SideBarItem } from '@components/domains/layouts/Sidebar/SideBaritem'

import { TButtonProps } from '@components/uiKit/Button'
import { Text } from '@components/uiKit/Text'

import { TRANS } from '@components/utils/types'

import { routing } from '@lib/routing'

type TSidebarHomeProps = Partial<TButtonProps>;

const SidebarSearch = (props: TSidebarHomeProps): React.ReactElement | null => {
  const {
    ...other
  } = props

  const { t } = useTranslation(TRANS.MAIN)


  const renderIcon = (isActiveRoute: boolean) => {
    if (!isActiveRoute) {
      return (
        <SearchIcon
          className='h-6 w-6' />
      )
    }

    return (
      <SearchIconSolid
        className='h-6 w-6'
      />
    )
  }

  return (
    <SideBarItem
      currPathName={routing.getSearchUrl()}>
      {
        isActiveRoute =>
          <Link
            href={routing.getSearchUrl()}
          >
            <a
              className={['flex', 'items-center', 'space-x-2'].join(' ')}>
              {
                renderIcon(isActiveRoute)
              }
              <Text
                cls={['sidebar-item-txt','font-bold']}>
                {t('sidebar.searchItem')}
              </Text>
            </a>
          </Link>
      }

    </SideBarItem>
  )
}

export { SidebarSearch }
