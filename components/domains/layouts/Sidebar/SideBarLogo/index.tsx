import Link from 'next/link'
import React from 'react'

import { Box, TBoxProps } from '@components/uiKit/Box'
import { Image } from '@components/uiKit/Image'

import { routing } from '@lib/routing'


type TSidebarLogoProps = Partial<TBoxProps>;

const SidebarLogo = (props: TSidebarLogoProps): React.ReactElement | null => {
  const {
    ...other
  } = props

  return (
    <Box
      cls={['mb-6']}
      {...other}>
      <Link
        href={routing.getIndexUrl()}
      >
        <a>
          <Image
            src='/images/spotify-logo-white.png'
            width='131px'
            height='40px'
            layout='intrinsic'
            alt='Logo-white'
          />
        </a>
      </Link>
    </Box>
  )
}

export { SidebarLogo }
