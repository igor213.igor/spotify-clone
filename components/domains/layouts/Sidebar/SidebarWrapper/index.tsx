import React from 'react'

import { Box, TBoxProps } from '@components/uiKit/Box'

type TSidebarWrapperProps = TBoxProps & {
  children: React.ReactNode;
}

export const WIDTH_SIDEBAR = '241'

const SidebarWrapper = (props: TSidebarWrapperProps): React.ReactElement | null => {
  const {
    children,
    ...other
  } = props


  return (
    <Box
      // style={{ width: WIDTH_SIDEBAR + 'px' }}
      // cls={['w-60']}
      {...other}>
      {
        children
      }
    </Box>
  )
}

export { SidebarWrapper }
