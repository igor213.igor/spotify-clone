import { HomeIcon } from '@heroicons/react/outline'
import HomeIconSolid from '@heroicons/react/solid/HomeIcon'
import { useTranslation } from 'next-i18next'
import Link from 'next/link'
import React from 'react'

import { SideBarItem } from '@components/domains/layouts/Sidebar/SideBaritem'

import { Button, TButtonProps } from '@components/uiKit/Button'
import { Text } from '@components/uiKit/Text'

import { TRANS } from '@components/utils/types'

import { routing } from '@lib/routing'

type TSidebarHomeProps = Partial<TButtonProps>;

const SidebarHome = (props: TSidebarHomeProps): React.ReactElement | null => {
  const {
    ...other
  } = props

  const { t } = useTranslation(TRANS.MAIN)


  const renderIcon = (isActiveRoute: boolean) => {
    if (!isActiveRoute) {
      return (
        <HomeIcon
          className='h-6 w-6' />
      )
    }

    return (
      <HomeIconSolid
        className='h-6 w-6'
      />
    )
  }


  return (
    <SideBarItem
      currPathName={routing.getIndexUrl()}>
      {
        isActiveRoute =>
          <Link
            href={routing.getIndexUrl()}
          >
            <a
              className={['flex', 'items-center', 'space-x-2'].join(' ')}>
              {
                renderIcon(isActiveRoute)
              }
              <Text
                cls={['sidebar-item-txt','font-bold']}>
                {t('sidebar.homeItem')}
              </Text>
            </a>
          </Link>
      }
    </SideBarItem>
  )
}

export { SidebarHome }
