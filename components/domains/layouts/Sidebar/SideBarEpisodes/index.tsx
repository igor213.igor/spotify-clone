import { RssIcon } from '@heroicons/react/outline'
import { useTranslation } from 'next-i18next'
import React from 'react'

import { SideBarItem } from '@components/domains/layouts/Sidebar/SideBaritem'

import { Button, TButtonProps } from '@components/uiKit/Button'

import { TRANS } from '@components/utils/types'

import { routing } from '@lib/routing'

type TSideBarEpisodesProps = Partial<TButtonProps>;

const SideBarEpisodes = (props: TSideBarEpisodesProps): React.ReactElement | null => {
  const {
    ...other
  } = props

  const { t } = useTranslation(TRANS.MAIN)

  const handleClick = () => {

  }


  return (
    <SideBarItem
      currPathName={routing.getEpisodesUrl()}>
      {
        isActiveRoute =>
          <Button
            startIcon={(
              <RssIcon
                className='h-6 w-6' />
            )}
            onClick={handleClick}
            cssBtn={'duration-500 hover:text-white'}
            {...other}>
            {t('sidebar.episodesItem')}
          </Button>
      }
    </SideBarItem>
  )
}

export { SideBarEpisodes }
