import { HeartIcon } from '@heroicons/react/outline'
import HeartIconSolid from '@heroicons/react/solid/HeartIcon'
import { useTranslation } from 'next-i18next'
import Link from 'next/link'
import React from 'react'

import { SideBarItem } from '@components/domains/layouts/Sidebar/SideBaritem'

import { TButtonProps } from '@components/uiKit/Button'
import { Text } from '@components/uiKit/Text'

import { TRANS } from '@components/utils/types'

import { routing } from '@lib/routing'

type TSideBarSongsProps = Partial<TButtonProps>;

const SideBarSongs = (props: TSideBarSongsProps): React.ReactElement | null => {
  const {
    ...other
  } = props

  const { t } = useTranslation(TRANS.MAIN)


  const renderIcon = (isActiveRoute: boolean) => {
    if (!isActiveRoute) {
      return (
        <HeartIcon
          className='h-6 w-6'
        />
      )
    }

    return (
      <HeartIconSolid
        className='h-6 w-6'
      />
    )
  }

  return (
    <SideBarItem
      currPathName={routing.getSongsUrl()}>
      {
        isActiveRoute =>
          <Link
            href={routing.getLikeSongsUrl()}
          >
            <a
              className={['flex', 'items-center', 'space-x-2'].join(' ')}>
              {
                renderIcon(isActiveRoute)
              }
              <Text
                cls={['sidebar-item-txt','font-bold']}>
                {t('sidebar.libraryItem')}
              </Text>
            </a>
          </Link>
      }
    </SideBarItem>
  )
}

export { SideBarSongs }
