import { PlusCircleIcon } from '@heroicons/react/outline'
import { useTranslation } from 'next-i18next'
import React from 'react'

import { SideBarItem } from '@components/domains/layouts/Sidebar/SideBaritem'

import { Button, TButtonProps } from '@components/uiKit/Button'

import { TRANS } from '@components/utils/types'

import { routing } from '@lib/routing'

type TSideBarCreatePlayListProps = Partial<TButtonProps>;

const SideBarCreatePlayList = (props: TSideBarCreatePlayListProps): React.ReactElement | null => {
  const {
    ...other
  } = props

  const { t } = useTranslation(TRANS.MAIN)

  const handleClick = () => {

  }


  return (
    <SideBarItem
      currPathName={routing.getLibraryUrl()}>
      {
        isActiveRoute =>
          <Button
            cssTxt='sidebar-item-txt font-bold'
            startIcon={(
              <PlusCircleIcon
                className='h-6 w-6' />
            )}
            onClick={handleClick}
            {...other}>
            {t('sidebar.createPlaylistItem')}
          </Button>
      }
    </SideBarItem>
  )
}

export { SideBarCreatePlayList }
