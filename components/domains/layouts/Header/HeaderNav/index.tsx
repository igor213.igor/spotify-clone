import React from 'react'

import { HeaderNavBackBtn } from '@components/domains/layouts/Header/buttons/HeaderNavBackBtn'
import { HeaderNavForwardBtn } from '@components/domains/layouts/Header/buttons/HeaderNavForwardBtn'

import { Box, TBoxProps } from '@components/uiKit/Box'

type THeaderNavProps = TBoxProps & {}

const HeaderNav = (props: THeaderNavProps): React.ReactElement | null => {
  const { ...other } = props

  const clsHeader = ['header-nav','flex','gap-4']

  return (
    <Box
      cls={clsHeader}
      {...other}
    >
      <HeaderNavBackBtn />

      <HeaderNavForwardBtn />

    </Box>
  )
}

export {
  HeaderNav,
}
