import { ChevronLeftIcon } from '@heroicons/react/outline'
import { useRouter } from 'next/router'
import React from 'react'

import { Box } from '@components/uiKit/Box'
import { IconButton, TIconButtonProps } from '@components/uiKit/IconButton'

type THeaderNavBackBtnProps = Partial<TIconButtonProps> & {}

const HeaderNavBackBtn = (props: THeaderNavBackBtnProps): React.ReactElement | null => {
  const { ...other } = props

  const router = useRouter()

  const handleClick = () => {
    router.back()
  }

  return (
    <IconButton
      onClick={handleClick}
      {...other}
    >
      <Box
        cls={['flex', 'items-center', 'justify-center', 'bg-neutral-800', 'rounded-full', 'h-8', 'w-8']}
      >
        <ChevronLeftIcon
          className='h-6 w-6 text-white' />
      </Box>
    </IconButton>
  )
}

export {
  HeaderNavBackBtn,
}
