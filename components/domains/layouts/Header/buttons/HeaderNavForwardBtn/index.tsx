import { ChevronRightIcon } from '@heroicons/react/outline'
import { useRouter } from 'next/router'
import React from 'react'

import { Box } from '@components/uiKit/Box'
import { IconButton, TIconButtonProps } from '@components/uiKit/IconButton'

type THeaderNavForwardBtnProps = Partial<TIconButtonProps> & {}

const HeaderNavForwardBtn = (props: THeaderNavForwardBtnProps): React.ReactElement | null => {
  const { ...other } = props

  const router = useRouter()

  const handleClick = () => {
    router.reload()
  }


  return (
    <IconButton
      onClick={handleClick}
      {...other}
    >
      <Box
        cls={['flex', 'items-center', 'justify-center', 'bg-neutral-800', 'rounded-full', 'h-8', 'w-8']}
      >
        <ChevronRightIcon
          className='h-6 w-6 text-white' />
      </Box>
    </IconButton>
  )
}

export {
  HeaderNavForwardBtn,
}
