import { useTranslation } from 'next-i18next'
import { useRouter } from 'next/router'
import React from 'react'

import { Button, TButtonProps } from '@components/uiKit/Button'

import { TRANS } from '@components/utils/types'

type THeaderTariffBtnProps = Partial<TButtonProps> & {}

const HeaderTariffBtn = (props: THeaderTariffBtnProps): React.ReactElement | null => {
  const { ...other } = props

  const { t } = useTranslation(TRANS.MAIN)

  const router = useRouter()

  const handleClick = () => {
    router.reload()
  }

  return (
    <Button
      cssBtn={['bg-neutral-800','rounded-full','px-8','py-2']}
      cssTxt={['text-white','text-xs','letter-spacing-1-76px','uppercase']}
      onClick={handleClick}
   >
      {t('header.tariffBtn')}
    </Button>
  )
}

export {
  HeaderTariffBtn,
}
