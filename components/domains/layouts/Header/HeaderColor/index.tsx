import React, { useState } from 'react'

import { Box, TBoxProps } from '@components/uiKit/Box'

import { useEventListener } from '@components/utils/hooks/useEventListener'

import { isServer } from '@lib/common'

type THeaderProps = TBoxProps;


const HeaderColor = (props: THeaderProps): React.ReactElement | null => {
  const { ...other } = props

  const [opacityState, setOpacityState] = useState(0)

  useEventListener(
    isServer() ? null : document.querySelector('.main'),
    'scroll',
    (el: React.UIEvent<HTMLDivElement>) => {
      const currElement = el.currentTarget
      const scroll = currElement.scrollTop

      const opacityFromScroll = scroll / 64
      setOpacityState(opacityFromScroll > 1 ? 1 : opacityFromScroll)
    }
  )

  return (
    <Box
      cls={['header-color', 'bg-neutral-800']}
      style={{ opacity: opacityState }}
      {...other}
    />
  )
}

export {
  HeaderColor,
}
