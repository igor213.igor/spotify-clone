import { signOut, useSession } from 'next-auth/react'
import React from 'react'

import { HeaderColor } from '@components/domains/layouts/Header/HeaderColor'
import { HeaderNav } from '@components/domains/layouts/Header/HeaderNav'

import { Box } from '@components/uiKit/Box'
import { UserAvatar } from '@components/uiKit/UserAvatar'

type THeaderProps = {
  children?: React.ReactNode
}

const Header = (props: THeaderProps): React.ReactElement | null => {

  const clsHeader = ['header', 'h-64px', 'z-10', 'sticky', 'py-4', 'px-8', 'top-0']

  const { data: session } = useSession()

  const renderUserAvatar = () => {
    const img = session?.user?.image || ''
    const name = session?.user?.name || ''

    return (
      <UserAvatar
        img={img}
        name={name}
        // @ts-ignore
        onClick={signOut}
      />
    )
  }

  return (
    <Box
      cls={clsHeader}
    >
      <HeaderColor />
      <Box
        cls={['flex', 'items-center', 'h-full', 'justify-between']}
        As='header'>

        <HeaderNav />

        <Box
          cls={['flex', 'gap-8']}>
          {
            renderUserAvatar()
          }
        </Box>
      </Box>
    </Box>
  )
}

export {
  Header,
}
