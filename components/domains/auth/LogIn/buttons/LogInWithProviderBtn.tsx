import { ClientSafeProvider, signIn } from 'next-auth/react'
import { useTranslation } from 'next-i18next'
import React from 'react'

import { Button, TButtonProps } from '@components/uiKit/Button'

import { TRANS } from '@components/utils/types'

import { routing } from '@lib/routing'

type TLogInWithProviderBtnProps = Partial<TButtonProps> & {
  provider: ClientSafeProvider
};

const LogInWithProviderBtn = (props: TLogInWithProviderBtnProps): React.ReactElement | null => {
  const {
    provider,
    ...other
  } = props

  const { t } = useTranslation(TRANS.MAIN)

  const handleClick = async () => {
    await signIn(provider.id, { callbackUrl: routing.getIndexUrl() })
  }

  return (
    <Button
      cssBtn='bg-[#18D860] text-white py-2 px-8 rounded-full hover:bg-[#0aa143] font-bold'
      onClick={handleClick}
      {...other}>

      {
        t('pages.logIn.loginWithBtn', {
          providerName: provider.name,
        })
      }

    </Button>
  )
}

export {
  LogInWithProviderBtn,
}
