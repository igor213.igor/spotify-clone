import { useTranslation } from 'next-i18next'
import React from 'react'

import { TNextAuthProviders } from '@Redux/types'

import { LogInWithProviderBtn } from '@components/domains/auth/LogIn/buttons/LogInWithProviderBtn'
import { Logo } from '@components/domains/common/Logo'

import { TRANS } from '@components/utils/types'

type TLogInProps = TNextAuthProviders;

const LogIn = (props: TLogInProps): React.ReactElement | null => {
  const {
    providers,
    ...other
  } = props

  const { t } = useTranslation(TRANS.MAIN)

  return (
    <div
      className='flex items-center flex-col w-2/5'>
      <Logo
        width='300px'
        height='105px'
        layout='intrinsic'
      />
      <div
        className='mt-2'>
        {
          Object.values(providers).map(provider => (
            <LogInWithProviderBtn
              key={provider.id}
              provider={provider}
            />
          ))
        }
      </div>
    </div>
  )
}

export {
  LogIn,
}
