import { useSession } from 'next-auth/react'
import React, { useEffect } from 'react'

import { asyncPlayListsActions } from '@Redux/domains/playlists/asyncActions'
import { useAppDispatch } from '@Redux/hooks'

import { useSpotify } from '@components/utils/hooks/useSpotify'

const HomeLoadData = (): React.ReactElement | null => {

  const spotify = useSpotify()

  const { data: session } = useSession({
    required: true,
  })

  const dispatch = useAppDispatch()

  useEffect(() => {
    if (spotify.getAccessToken()) {
      dispatch(asyncPlayListsActions.getUserPlayLists())
    }
  }, [spotify, session])


  return null
}

export {
  HomeLoadData,
}
