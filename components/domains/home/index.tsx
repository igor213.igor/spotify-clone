import React from 'react'

import { HomeContent } from '@components/domains/home/HomeContent'

import { TBoxProps } from '@components/uiKit/Box'

type THomeIndexProps = TBoxProps

const HomeIndex = (props: THomeIndexProps): React.ReactElement | null => {
  const { ...other } = props

  return (
      <HomeContent />
  )
}

export {
  HomeIndex,
}
