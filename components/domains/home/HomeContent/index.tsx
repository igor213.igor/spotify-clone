import React from 'react'

import { Box } from '@components/uiKit/Box'

type THomeContentProps = {}

const HomeContent = (props: THomeContentProps): React.ReactElement | null => {
  const { ...other } = props

  const rrr = Array.from(Array(300).keys())


  return (
    <Box
      cls={['px-8']}>
      {
        rrr.map(item => (
          <p
            key={item.toString()}
            className='text-white'>
            Playlist more ...
          </p>
        ))
      }
    </Box>
  )
}

export {
  HomeContent,
}
