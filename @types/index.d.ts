import { TAppState } from '@Redux/types'

declare global {
  interface Window {
    getState: () => TAppState;
  }
}
