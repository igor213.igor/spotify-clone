import { isEqual } from 'lodash'
import { createSelector } from 'reselect'

import { playListSelectors } from '@Redux/domains/playlist/selectors'
import { TPlayListModel } from '@Redux/domains/playlist/types'
import { playListsSelectors } from '@Redux/domains/playlists/selectors'

const getPlayListByCurrId = createSelector(
  [
    playListsSelectors.getCurrId,
    playListSelectors.getEntities,
  ],
  (currId, entities) => {
    if (!currId) {
      return
    }

    return entities[currId] as TPlayListModel
  }
)

const getHasPlayList = createSelector(
  [
    getPlayListByCurrId,
  ],
  (playList) => {
    return !!playList
  }
)

const getTracks = createSelector(
  [
    getPlayListByCurrId,
  ],
  (playList) => {
    if (!playList) {
      return []
    }

    return playList.tracks.items
  }
)

const getTracksIds = createSelector(
  [
    getTracks,
  ],
  (tracks) => {
    if (!tracks) {
      return []
    }

    return tracks.map(item => item.track.id)
  }
)

const getTrackById = createSelector(
  [
    getTracks,
  ],
  (tracks) => (id: string) => {
    if (!id || !tracks.length) {
      return
    }

    const findItem = tracks.find(item => isEqual(item.track.id, id))

    if (!findItem) {
      return
    }

    return findItem.track

  }
)


const hasTrackById = createSelector(
  getTrackById,
  (getTrackByIdSelector) => (id: string) => {
    return !!getTrackByIdSelector(id)
  }
)


const getAlbumNameFromTrack = createSelector(
  getTrackById,
  (getTrackByIdSelector) => (id: string) => {
    const track = getTrackByIdSelector(id)

    if (!track) {
      return ''
    }

    return track.album.name


  }
)

const getDurationFromTrack = createSelector(
  getTrackById,
  (getTrackByIdSelector) => (id: string) => {
    const track = getTrackByIdSelector(id)

    if (!track) {
      return 0
    }

    return track.duration_ms
  }
)

const getArtistsFromTrack = createSelector(
  getTrackById,
  (getTrackByIdSelector) => (id: string) => {
    const track = getTrackByIdSelector(id)

    if (!track) {
      return []
    }

    return track.artists
  }
)


const getNameFromTrack = createSelector(
  getTrackById,
  (getTrackByIdSelector) => (id: string) => {
    const track = getTrackByIdSelector(id)

    if (!track) {
      return []
    }

    return track.name
  }
)

const getAlbumImagesFromTrack = createSelector(
  getTrackById,
  (getTrackByIdSelector) => (id: string) => {
    const track = getTrackByIdSelector(id)

    if (!track) {
      return []
    }

    return track.album.images
  }
)


const getUriFromTrack = createSelector(
  getTrackById,
  (getTrackByIdSelector) => (id: string) => {
    const track = getTrackByIdSelector(id)

    if (!track) {
      return []
    }

    return track.uri
  }
)

const playlistRepoSelectors = {
  getPlayListByCurrId,
  getHasPlayList,
  getTracksIds,
  getTrackById,
  hasTrackById,
  getAlbumNameFromTrack,
  getDurationFromTrack,
  getArtistsFromTrack,
  getNameFromTrack,
  getAlbumImagesFromTrack,
  getUriFromTrack,
}

export {
  playlistRepoSelectors,
}
