import { AUTH } from '@components/utils/types'

type TAuthModels = {
  accessToken: string;
  expires: string;
  refreshToken: string;
  status: AUTH;
}


export type {
  TAuthModels,
}
