const authActionsTypes = {
  // sign in
  SPOTIFY_OAUTH_PENDING: 'auth/spotifyOauth/pending',
  SPOTIFY_OAUTH_FULFILLED: 'auth/spotifyOauth/fulfilled',
  SPOTIFY_OAUTH_REJECTED: 'auth/spotifyOauth/rejected',

} as const;

export {
  authActionsTypes,
}
