import { createAction } from '@reduxjs/toolkit'

import { authActionsTypes } from '@Redux/domains/auth/actionsTypes'

const spotifyOAuthFulfilled = createAction(authActionsTypes.SPOTIFY_OAUTH_FULFILLED)


const authActions = {
  spotifyOAuthFulfilled,
}

export {
  authActions,
}
