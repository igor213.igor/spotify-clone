import { createSlice, PayloadAction } from '@reduxjs/toolkit'

import { TAuthModels } from '@Redux/domains/auth/modelsTypes'


type TAuthState = TAuthModels | {};

const authInitialState: TAuthState = {}

const nameAuthReducer = 'auth'


const authReducerSlice = createSlice({
  name: nameAuthReducer,
  initialState: authInitialState,
  reducers: {
    spotifyOauth(state, action: PayloadAction<TAuthModels>) {
      return action.payload
    },
  },
})

const { actions: authActions, reducer } = authReducerSlice


export {
  authActions,
}

export default reducer
