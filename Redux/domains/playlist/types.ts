import { spotifyApi } from '@api/spotify'

import { typesHelpers } from '@lib/common/types'


type TPlayListModel = typesHelpers.PropType<Awaited<ReturnType<typeof spotifyApi.getPlayList>>, 'body'>

export type {
  TPlayListModel,
}
