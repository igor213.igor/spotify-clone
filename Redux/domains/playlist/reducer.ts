import { createSlice } from '@reduxjs/toolkit'

import { asyncPlayListActions } from '@Redux/domains/playlist/asyncActions'
import { playListEntityAdapter } from '@Redux/domains/playlist/entityAdapter'
import { TPlaylistState } from '@Redux/domains/playlist/reducersTypes'
import { REDUCERS_NAME } from '@Redux/domains/types'

import { FETCH_STATUS } from '@components/utils/types'

const nameReducer = REDUCERS_NAME.PLAYLIST

const initialState = playListEntityAdapter.getInitialState<TPlaylistState>({
  ids: [],
  entities: {},
  error: null,
  status: FETCH_STATUS.INITIAL,
})

const reducerSlice = createSlice({
  name: nameReducer,
  initialState,
  reducers: {},
  extraReducers(builder) {
    builder
      .addCase(asyncPlayListActions.getPlaylist.pending, (state) => {
        state.status = FETCH_STATUS.LOADING
      })
      .addCase(asyncPlayListActions.getPlaylist.fulfilled, (state, action) => {
        state.status = FETCH_STATUS.SUCCEEDED
        playListEntityAdapter.setOne(state, action.payload)
      })
      .addCase(asyncPlayListActions.getPlaylist.rejected, (state, action) => {
        state.status = FETCH_STATUS.FAILED
        state.error = action.error
      })
  },
})

const { reducer } = reducerSlice

export default reducer
