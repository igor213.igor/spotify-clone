import { playListEntityAdapter } from '@Redux/domains/playlist/entityAdapter'
import { TRejectedAsyncActions } from '@Redux/types'

import { TFetchStatus } from '@components/utils/types'

type TPlaylistState = ReturnType<typeof playListEntityAdapter.getInitialState>
  & TRejectedAsyncActions
  & TFetchStatus;

export type {
  TPlaylistState,
}
