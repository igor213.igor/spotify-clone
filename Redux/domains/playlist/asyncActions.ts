import { createAsyncThunk } from '@reduxjs/toolkit'

import { spotifyApi } from '@api/spotify'

import { playListActionsTypes } from '@Redux/domains/playlist/actionsTypes'


const getPlaylist = createAsyncThunk(
  playListActionsTypes.GET_PLAY_LIST,
  async (id: string) => {
    const response = await spotifyApi.getPlayList(id)

    return response.body
  }
)

const asyncPlayListActions = {
  getPlaylist,
}

export {
  asyncPlayListActions,
}
