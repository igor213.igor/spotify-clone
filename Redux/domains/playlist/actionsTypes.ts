const playListActionsTypes = {

  GET_PLAY_LIST: 'playList/get',

} as const;

export {
  playListActionsTypes,
}
