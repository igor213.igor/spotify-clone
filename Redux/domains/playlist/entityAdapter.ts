import { createEntityAdapter } from '@reduxjs/toolkit'

import { TPlayListModel } from '@Redux/domains/playlist/types'

const playListEntityAdapter = createEntityAdapter<TPlayListModel>()

export {
  playListEntityAdapter,
}
