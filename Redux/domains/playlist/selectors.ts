import { createSelector } from 'reselect'

import { TAppState } from '@Redux/types'

import { TEntityID } from '@components/utils/types'


const getPlayList = ({ playlist }: TAppState) => playlist


const getEntities = createSelector(
  getPlayList,
  (playlists) => playlists.entities
)



const getIds = createSelector(
  getPlayList,
  (playlists) => playlists.ids
)


const getById = createSelector(
  getEntities,
  (entities) => (id: TEntityID | undefined) => {
    if (!id) {
      return
    }

    return entities[id.toString()]
  }
)

const playListSelectors = {
  getEntities,
  getIds,
  getById,

}

export {
  playListSelectors,
}
