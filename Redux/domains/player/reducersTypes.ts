import { playerEntityAdapter } from '@Redux/domains/player/entityAdapter'
import { TCurrIdState, TRejectedAsyncActions } from '@Redux/types'

import { TFetchStatus } from '@components/utils/types'

type TPlayerState = ReturnType<typeof playerEntityAdapter.getInitialState>
  & TRejectedAsyncActions
  & TFetchStatus
  & TCurrIdState
  & {
  hasPlay: boolean
}
  & {
  volume: number
}

export type {
  TPlayerState,
}
