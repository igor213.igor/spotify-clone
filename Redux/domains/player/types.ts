import { spotifyApi } from '@api/spotify'

import { typesHelpers } from '@lib/common/types'

type TPlayerModel = typesHelpers.PropType<Awaited<ReturnType<typeof spotifyApi.getTrackInfo>>, 'body'>

export type {
  TPlayerModel,
}
