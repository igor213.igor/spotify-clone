import { createAsyncThunk } from '@reduxjs/toolkit'

import { spotifyApi } from '@api/spotify'

import { playerActionsTypes } from '@Redux/domains/player/actionsTypes'


const getTrackInfo = createAsyncThunk(
  playerActionsTypes.GET_TRACK_INFO,
  async (trackId: string) => {
    const response = await spotifyApi.getTrackInfo(trackId)

    return response.body
  }
)

const asyncPlayerActions = {
  getTrackInfo,
}

export {
  asyncPlayerActions,
}
