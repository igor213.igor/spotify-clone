import { createEntityAdapter } from '@reduxjs/toolkit'

import { TPlayerModel } from '@Redux/domains/player/types'

const playerEntityAdapter = createEntityAdapter<TPlayerModel>()

export {
  playerEntityAdapter,
}
