const playerActionsTypes = {

  GET_TRACK_INFO: 'player/trackInfo/get',

} as const;

export {
  playerActionsTypes,
}
