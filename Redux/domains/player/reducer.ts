import { createSlice, PayloadAction } from '@reduxjs/toolkit'

import { asyncPlayerActions } from '@Redux/domains/player/asyncActions'
import { playerEntityAdapter } from '@Redux/domains/player/entityAdapter'
import { TPlayerState } from '@Redux/domains/player/reducersTypes'
import { REDUCERS_NAME } from '@Redux/domains/types'

import { FETCH_STATUS } from '@components/utils/types'

const initialState = playerEntityAdapter.getInitialState<TPlayerState>({
  ids: [],
  entities: {},
  currId: '',
  error: null,
  status: FETCH_STATUS.INITIAL,
  volume: 0.3,
  hasPlay: false,
})

const nameReducer = REDUCERS_NAME.PLAYER

const reducerSlice = createSlice({
  name: nameReducer,
  initialState: initialState,
  reducers: {
    setCurrId: (state, action: PayloadAction<string>) => {
      state.currId = action.payload
    },
    setVolume: (state, action: PayloadAction<number>) => {
      state.volume = action.payload
    },
    setPlay: (state, action: PayloadAction<boolean>) => {
      state.hasPlay = action.payload
    },
  },
  extraReducers(builder) {
    builder
      .addCase(asyncPlayerActions.getTrackInfo.pending, (state) => {
        state.status = FETCH_STATUS.LOADING
      })
      .addCase(asyncPlayerActions.getTrackInfo.fulfilled, (state, action) => {
        state.status = FETCH_STATUS.SUCCEEDED
        playerEntityAdapter.setOne(state, action.payload)
      })
      .addCase(asyncPlayerActions.getTrackInfo.rejected, (state, action) => {
        state.status = FETCH_STATUS.FAILED
        state.error = action.error
      })
  },
})

const { actions: playerActions, reducer } = reducerSlice

export {
  playerActions,
}

export default reducer
