import { createSelector } from 'reselect'

import { TAppState } from '@Redux/types'

import { TEntityID } from '@components/utils/types'


const getPlayer = ({ player }: TAppState) => player


const getEntities = createSelector(
  getPlayer,
  (player) => player.entities
)


const getIds = createSelector(
  getPlayer,
  (player) => player.ids
)


const getById = createSelector(
  getEntities,
  (entities) => (id: TEntityID | undefined) => {
    if (!id) {
      return
    }

    return entities[id.toString()]
  }
)

const getCurrId = createSelector(
  getPlayer,
  (player) => player.currId
)

const getHasPlay = createSelector(
  getPlayer,
  (player) => player.hasPlay
)

const getVolume = createSelector(
  getPlayer,
  (player) => player.volume
)

const getFetchStatus = createSelector(
  getPlayer,
  (player) => player.status
)



const getFromCurrId = createSelector(
  [getCurrId, getById],
  (currId, getByIdSector) => {
    if (!currId) {
      return
    }

    return getByIdSector(currId)
  }
)

/**
 * Selector from entity
 */

const getAlbumImagesFromTrack = createSelector(
  getFromCurrId,
  (track) => {

    if (!track) {
      return []
    }

    return track.album.images
  }
)

const getNameFromTrack = createSelector(
  getFromCurrId,
  (track) => {

    if (!track) {
      return ''
    }

    return track.name
  }
)

const getArtistsFromTrack = createSelector(
  getFromCurrId,
  (track) => {

    if (!track) {
      return []
    }

    return track.artists
  }
)


const getAudioPreviewFromTrack = createSelector(
  getFromCurrId,
  (track) => {

    if (!track) {
      return ''
    }

    return track.preview_url || ''
  }
)


const playerSelectors = {
  getEntities,
  getIds,
  getById,
  getCurrId,
  getHasPlay,
  getVolume,
  getFetchStatus,

  getFromCurrId,
  getAlbumImagesFromTrack,
  getNameFromTrack,
  getArtistsFromTrack,
  getAudioPreviewFromTrack,

}


export {
  playerSelectors,
}
