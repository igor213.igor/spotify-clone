import { createAsyncThunk } from '@reduxjs/toolkit'

import { spotifyApi } from '@api/spotify'

import { playListsActionsTypes } from '@Redux/domains/playlists/actionsTypes'

const getUserPlayLists = createAsyncThunk(
  playListsActionsTypes.GET_MANY_PLAY_LISTS,
  async () => {
    const response = await spotifyApi.getUserPlayLists()

    return response.body.items
  }
)

const asyncPlayListsActions = {
  getUserPlayLists,
}

export {
  asyncPlayListsActions,
}
