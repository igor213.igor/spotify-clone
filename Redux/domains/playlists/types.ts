import { spotifyApi } from '@api/spotify'

import { typesHelpers } from '@lib/common/types'


type TPlaylistsModels = typesHelpers.PropType<
  typesHelpers.PropType<Awaited<ReturnType<typeof spotifyApi.getUserPlayLists>>, 'body'>,
  'items'>

export type {
  TPlaylistsModels,
}
