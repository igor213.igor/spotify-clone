import { createSelector } from 'reselect'

import { TAppState } from '@Redux/types'

import { TEntityID } from '@components/utils/types'


const getPlayLists = ({ playlists }: TAppState) => playlists


const getEntities = createSelector(
  getPlayLists,
  (playlists) => playlists.entities
)

const getCurrId = createSelector(
  getPlayLists,
  (playlists) => playlists.currId
)


const getIds = createSelector(
  getPlayLists,
  (playlists) => playlists.ids
)


const getById = createSelector(
  getEntities,
  (entities) => (id: TEntityID | undefined) => {
    if (!id) {
      return
    }

    return entities[id.toString()]
  }
)


const getFromCurrId = createSelector(
  [getCurrId, getById],
  (currId, getByIdSector) => {
    if (!currId) {
      return
    }

    return getByIdSector(currId)
  }
)


/**
 * Selector from entity
 */
const getOwnerFromCurrId = createSelector(
  [getFromCurrId],
  (entity) => {
    if (!entity) {
      return
    }

    return entity.owner
  }
)

const getNameFromCurrId = createSelector(
  [getFromCurrId],
  (entity) => {
    if (!entity) {
      return
    }

    return entity.name
  }
)

const getImagesFromCurrId = createSelector(
  [getFromCurrId],
  (entity) => {
    if (!entity) {
      return []
    }

    return entity.images
  }
)


const playListsSelectors = {
  getEntities,
  getIds,
  getById,
  getCurrId,


  //Selector entity
  getFromCurrId,
  getOwnerFromCurrId,
  getNameFromCurrId,
  getImagesFromCurrId,

}

export {
  playListsSelectors,
}
