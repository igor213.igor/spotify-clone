import { createSlice, PayloadAction } from '@reduxjs/toolkit'

import { asyncPlayListsActions } from '@Redux/domains/playlists/asyncActions'
import { playListsEntityAdapter } from '@Redux/domains/playlists/entityAdapter'
import { TPlaylistsState } from '@Redux/domains/playlists/reducersTypes'
import { REDUCERS_NAME } from '@Redux/domains/types'

import { FETCH_STATUS } from '@components/utils/types'

const initialState = playListsEntityAdapter.getInitialState<TPlaylistsState>({
  ids: [],
  entities: {},
  currId: '',
  error: null,
  status: FETCH_STATUS.INITIAL,
})

const nameReducer = REDUCERS_NAME.PLAYLISTS

const reducerSlice = createSlice({
  name: nameReducer,
  initialState: initialState,
  reducers: {
    setCurrId: (state, action: PayloadAction<string>) => {
      state.currId = action.payload
    },
  },
  extraReducers(builder) {
    builder
      .addCase(asyncPlayListsActions.getUserPlayLists.pending, (state) => {
        state.status = FETCH_STATUS.LOADING
      })
      .addCase(asyncPlayListsActions.getUserPlayLists.fulfilled, (state, action) => {
        state.status = FETCH_STATUS.SUCCEEDED
        playListsEntityAdapter.setAll(state, action.payload)
      })
      .addCase(asyncPlayListsActions.getUserPlayLists.rejected, (state, action) => {
        state.status = FETCH_STATUS.FAILED
        state.error = action.error
      })
  },
})

const { actions: playListsActions, reducer } = reducerSlice

export {
  playListsActions,
}

export default reducer
