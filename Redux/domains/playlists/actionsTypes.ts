const playListsActionsTypes = {

  GET_MANY_PLAY_LISTS: 'playLists/getMany',

} as const;

export {
  playListsActionsTypes,
}
