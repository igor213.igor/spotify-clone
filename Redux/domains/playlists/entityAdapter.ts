import { createEntityAdapter } from '@reduxjs/toolkit'

import { TPlaylistsModels } from '@Redux/domains/playlists/types'

const playListsEntityAdapter = createEntityAdapter<TPlaylistsModels[0]>({
  selectId: (playList) => playList.id,
  sortComparer: (a, b) => b.name.localeCompare(a.name),
})

export {
  playListsEntityAdapter,
}
