import { playListsEntityAdapter } from '@Redux/domains/playlists/entityAdapter'
import { TRejectedAsyncActions } from '@Redux/types'

import { TFetchStatus } from '@components/utils/types'

type TPlaylistsState = ReturnType<typeof playListsEntityAdapter.getInitialState>
  & TRejectedAsyncActions
  & TFetchStatus
  & {
  currId: string;
}

export type {
  TPlaylistsState,
}
