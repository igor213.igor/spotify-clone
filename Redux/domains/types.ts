enum REDUCERS_NAME {
  PLAYLISTS = 'playlists',
  PLAYLIST = 'playlist',
  TRACK = 'track',
  PLAYER = 'player',
}


export {
  REDUCERS_NAME,
}
