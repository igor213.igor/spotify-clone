import authReducer from '@Redux/domains/auth/reducers'
import playerReducer from '@Redux/domains/player/reducer'
import playlistReducer from '@Redux/domains/playlist/reducer'
import playlistsReducer from '@Redux/domains/playlists/reducer'

const rootReducer = {
  auth: authReducer,
  playlists: playlistsReducer,
  playlist: playlistReducer,
  player: playerReducer,
}

export {
  rootReducer,
}
