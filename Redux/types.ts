import { SerializedError } from '@reduxjs/toolkit'
import { ClientSafeProvider, SessionProviderProps } from 'next-auth/react'
import type { AppProps } from 'next/app'

import { reduxStore } from '@Redux/lib/configureStore'

import { TNextPageWithLayout } from '@lib/next/types'

type TAppProps = Omit<AppProps, 'pageProps' | 'Component'> & {
  pageProps: {
    session: SessionProviderProps['session']
  },
  Component: TNextPageWithLayout
};

type TAppDispatch = typeof reduxStore.dispatch

type TAppState = ReturnType<typeof reduxStore.getState>

type TErrorAsyncActions = SerializedError

type TRejectedAsyncActions = {
  error: TErrorAsyncActions | null
}

type TCurrIdState = {
  currId: string
}

type TNextAuthProviders = {
  providers: ClientSafeProvider[]
};


export type {
  TAppDispatch,
  TAppState,
  TAppProps,
  TRejectedAsyncActions,
  TCurrIdState,
  TNextAuthProviders,
}

