import { configureStore } from '@reduxjs/toolkit'
import reduxLogger from 'redux-logger'

import { rootReducer } from '@Redux/domains/rootReducer'

import { isProd, isServer } from '@lib/common'


const getConfigureStore = () => {

  const store = configureStore({
    reducer: rootReducer,
    middleware: getDefaultMiddleware =>{
     return  getDefaultMiddleware()
       .concat(reduxLogger)
    },
    devTools: !isProd,
    preloadedState: {},
  })

  if (!isServer()) {
    window.getState = store.getState
  }


  return store
}

const reduxStore = getConfigureStore()


export {
  reduxStore,
}
