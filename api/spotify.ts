import { TEntityID } from '@components/utils/types'

import { nextAuthHelper, TAppJWT } from '@lib/nextAuthHelper'
import { spotifyLib } from '@lib/spotify'

const getRefreshAccessToken = async (token: TAppJWT) => {

  if (!token.accessToken || !token.refreshToken) {
    return {}
  }

  spotifyLib.setRefreshToken(token.refreshToken)

  try {
    const result = await spotifyLib.refreshAccessToken()
    const { body: newToken } = result

    return {
      user: token.user,
      accessToken: newToken.access_token,
      refreshToken: newToken.refresh_token ?? token.refreshToken,
      accessTokenExpires: Date.now() + nextAuthHelper.getAccessTokenExpires(newToken.expires_in),
    }
  } catch (e: any) {
    console.log('ERROR REFRESHED TOKEN', e.message)

    return {
      ...token,
      error: e.message,
    }
  }
}


const getUserPlayLists = async () => {
  const result = await spotifyLib.getUserPlaylists()

  return result
}

const getPlayList = async (id: TEntityID) => {
  const result = await spotifyLib.getPlaylist(id.toString())

  return result
}


const setPlayTrack = async (uris: string[]) => {

  const result = await spotifyLib.play({
    uris,
  })

  return result
}

const getTrackInfo = async (trackId: string) => {

  const result = await spotifyLib.getTrack(trackId)

  return result
}


const spotifyApi = {
  getRefreshAccessToken,
  getUserPlayLists,
  getPlayList,
  setPlayTrack,
  getTrackInfo,
}

export {
  spotifyApi,
}
