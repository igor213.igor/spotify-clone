import { GetServerSideProps } from 'next'
import { serverSideTranslations } from 'next-i18next/serverSideTranslations'
import React from 'react'

import { MainLayout } from '@components/domains/layouts/MainLayout'
import { PlayList } from '@components/domains/playlists/PlayList'
import { PlayListLoadData } from '@components/domains/playlists/PlayList/utils/PlayListLoadData'

import { PageLayout } from '@components/uiKit/PageLayout'

import { LOCALE, TRANS } from '@components/utils/types'

import { TNextPageWithLayout } from '@lib/next/types'

const PlayListPage: TNextPageWithLayout = () => {
  return (
    <PageLayout
      cls={['text-white']}>
      <PlayList />
      <PlayListLoadData />
    </PageLayout>
  )
}

PlayListPage.getLayout = function getLayout(page: React.ReactElement) {
  return (
    <MainLayout>
      {page}
    </MainLayout>
  )
}

export const getServerSideProps: GetServerSideProps = async (ctx) => {
  const { locale } = ctx
  const currLocale = locale || LOCALE.EN


  return {
    props: {
      ...(await serverSideTranslations(currLocale, [TRANS.MAIN])),
    },
  }
}


export default PlayListPage
