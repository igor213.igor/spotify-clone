import NextAuth from 'next-auth'
import SpotifyProvider from 'next-auth/providers/spotify'

import { spotifyApi } from '@api/spotify'

import { nextAuthHelper, TAppJWT, TAppSession } from '@lib/nextAuthHelper'
import { routing } from '@lib/routing'
import { AUTH_URL } from '@lib/spotify'

export default NextAuth({
  providers: [
    SpotifyProvider({
      clientId: process.env.NEXT_PUBLIC_SPOTIFY_CLIENT_ID || '',
      clientSecret: process.env.NEXT_PUBLIC_SPOTIFY_SECRET_TOKEN || '',
      authorization: AUTH_URL,
    }),
  ],
  pages: {
    signIn: routing.getLogInUrl(),
  },
  secret: process.env.JWT_SECRET,
  callbacks: {
    async jwt({ token, user, account }) {
      console.log(token,'-----------ЗАХОДИМ В JWT------------')

      if (account && user) {
        return nextAuthHelper.getAppJWT(user, account)
      }

      const currToken = token as TAppJWT

      if (Date.now() < currToken.accessTokenExpires) {
        console.log('TOKEN IS VALID')

        return token
      }

      console.log('TOKEN IS INVALID, REFRESHED IT')

      return await spotifyApi.getRefreshAccessToken(currToken)
    },
    async session({ session, token }) {
      console.log('-----------ЗАХОДИМ В СЕССИЮ------------')

      const currToken = token as TAppJWT

      const currSession: TAppSession = {
        ...session,
        accessToken: currToken.accessToken,
        refreshToken: currToken.refreshToken,
        user: currToken.user,
      }

      return currSession
    },
  },
  debug: true,
})
