import { GetServerSideProps } from 'next'
import { getProviders } from 'next-auth/react'
import { serverSideTranslations } from 'next-i18next/serverSideTranslations'

import { TAppPage, TNextAuthProviders } from '@Redux/types'

import { LogIn } from '@components/domains/auth/LogIn'

import { LOCALE, TRANS } from '@components/utils/types'

const LogInPage: TAppPage<{}, TNextAuthProviders> = (props) => {
  const { providers } = props

  return (
    <div
      className='flex flex-col items-center bg-black h-screen overflow-hidden w-full justify-center'>
      <LogIn
        providers={providers}
      />
    </div>
  )
}

export const getServerSideProps: GetServerSideProps = async (ctx) => {
  const { locale } = ctx
  const currLocale = locale || LOCALE.EN

  const providers = await getProviders()

  return {
    props: {
      providers,
      ...(await serverSideTranslations(currLocale, [TRANS.MAIN])),
    },
  }
}


export default LogInPage
