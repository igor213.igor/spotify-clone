import { GetServerSideProps } from 'next'
import { serverSideTranslations } from 'next-i18next/serverSideTranslations'
import React from 'react'

import { HomeIndex } from '@components/domains/home'
import { HomeLoadData } from '@components/domains/home/utils/HomeLoadData'
import { MainLayout } from '@components/domains/layouts/MainLayout'

import { PageLayout } from '@components/uiKit/PageLayout'

import { LOCALE, TRANS } from '@components/utils/types'

import { TNextPageWithLayout } from '@lib/next/types'

const Home: TNextPageWithLayout = () => {
  return (
    <PageLayout>
      <HomeIndex />
      <HomeLoadData />
    </PageLayout>
  )
}

Home.getLayout = function getLayout(page: React.ReactElement) {
  return (
    <MainLayout>
      {page}
    </MainLayout>
  )
}


export const getServerSideProps: GetServerSideProps = async (ctx) => {
  const { locale, req } = ctx
  const currLocale = locale || LOCALE.EN

  return {
    props: {
      ...(await serverSideTranslations(currLocale, [TRANS.MAIN])),
    },
  }
}

export default Home
