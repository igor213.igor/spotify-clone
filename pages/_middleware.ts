import { getToken } from 'next-auth/jwt'
import type { NextRequest, NextFetchEvent } from 'next/server'
import { NextResponse } from 'next/server'

import { routing } from '@lib/routing'

export async function middleware(req: NextRequest) {
  const { pathname } = req.nextUrl

  console.log('-----------ЗАХОДИМ В MIDDLEWARE------------')

  if (pathname.startsWith(routing.getStaticImagesUrl())) {
    return NextResponse.next()
  }

  // @ts-ignore
  const token = await getToken({ req, secret: process.env.JWT_SECRET })

  if (pathname.startsWith(routing.getApiAuthUrl()) || token) {
    return NextResponse.next()
  }

  const loginUrl = routing.getLogInUrl()

  if (!token && pathname !== loginUrl) {
    return NextResponse.redirect(new URL(loginUrl, req.url))
  }
}
