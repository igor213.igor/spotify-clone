import '../styles/globals.css'
import { SessionProvider } from 'next-auth/react'
import { appWithTranslation } from 'next-i18next'
import { Provider } from 'react-redux'

import { reduxStore } from '@Redux/lib/configureStore'
import { TAppProps } from '@Redux/types'

function MyApp(props: TAppProps) {
  const { Component, pageProps } = props
  const { session, ...otherProps } = pageProps

  const getLayout = Component.getLayout ?? ((page) => page)

  return (
    <SessionProvider
      session={session}>
      <Provider
        store={reduxStore}>
        {
          getLayout(
            <Component
              {...otherProps}
            />
          )
        }
      </Provider>
    </SessionProvider>
  )
}

export default appWithTranslation(MyApp)
