const getIndexUrl = () => '/'

const getSearchUrl = () => '/search'

const getLibraryUrl = () => '/collection/playlists'

const getLikeSongsUrl = () => '/collection/tracks'

const getSongsUrl = () => '/songs'

const getEpisodesUrl = () => '/episodes'

const getLogInUrl = () => '/log-in'

const getStaticImagesUrl = () => '/images'


const getPlaylistUrlTemplate = () => '/playlist/[id]'
const getPlaylistUrl = (id: string) => getPlaylistUrlTemplate()
  .replace('[id]', id)

const getSpotifyUrl = () => 'https://accounts.spotify.com'

const getSpotifyUrlAuth = () => getSpotifyUrl() + '/authorize'


const getApiUrl = () => '/api'
const getApiAuthUrl = () => getApiUrl() + '/auth'

const routing = {
  getSpotifyUrl,
  getSpotifyUrlAuth,
  getLogInUrl,
  getIndexUrl,
  getStaticImagesUrl,
  getSearchUrl,
  getLibraryUrl,
  getSongsUrl,
  getEpisodesUrl,
  getLikeSongsUrl,

  getPlaylistUrlTemplate,
  getPlaylistUrl,

  getApiUrl,
  getApiAuthUrl,
}
export {
  routing,
}
