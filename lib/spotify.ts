import SpotifyWebApi from 'spotify-web-api-node'

import { routing } from '@lib/routing'

const scopes = [
  'playlist-read-private',
  'playlist-read-collaborative',
  'streaming',
  'user-read-private',
  'user-read-email',
  'user-library-read',
  'user-top-read',
  'user-read-playback-state',
  'user-modify-playback-state',
  'user-read-currently-playing',
  'user-read-recently-played',
  'user-follow-read',
].join(',')

const params = {
  scope: scopes,
}

const queryParams = new URLSearchParams(params)

const AUTH_URL = routing.getSpotifyUrlAuth() + '?' + queryParams.toString()

const spotifyLib = new SpotifyWebApi({
  clientId: process.env.NEXT_PUBLIC_SPOTIFY_CLIENT_ID || '',
  clientSecret: process.env.NEXT_PUBLIC_SPOTIFY_SECRET_TOKEN || '',
})


export {
  AUTH_URL,
  spotifyLib,
}
