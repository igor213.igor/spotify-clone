import { ENVIRONMENT } from '@components/utils/types'

const isServer = () => typeof window === 'undefined';


const isProd = process.env.NODE_ENV === ENVIRONMENT.PRODUCTION;

export {
  isServer,
  isProd,
}
