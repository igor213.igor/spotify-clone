import { DateTime } from 'luxon'

const getMinutesAndSecondsFromMillis = (mls: number) => {

  return DateTime.fromMillis(mls).toFormat('m:ss');

}

const dateHelpers = {
  getMinutesAndSecondsFromMillis,
}

export {
  dateHelpers,
}

