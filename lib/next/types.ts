import { NextComponentType, NextPageContext, NextPage } from 'Next'
import { ClientSafeProvider } from 'next-auth/react'
import React from 'react'

type TPageContext = NextPageContext;

type TAppPage<TInitialProps, TProps> = NextComponentType<TPageContext, TInitialProps, TInitialProps & TProps>;

type TNextAuthProviders = {
  providers: ClientSafeProvider[]
};


type TNextPageWithLayout = NextPage & {
  getLayout?: (page: React.ReactElement) => React.ReactNode
}


export type {
  TAppPage,
  TNextAuthProviders,
  TNextPageWithLayout,
}
