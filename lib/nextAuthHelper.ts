import type { Account, Session, User } from 'next-auth/core/types'

type TAppJWT = {
  accessToken?: string,
  refreshToken?: string,
  accessTokenExpires: number,
  user: User,
}

type TAppSession = Session & {
  accessToken?: string,
  refreshToken?: string,
  user: TAppJWT['user'],
}

const getAccessTokenExpires = (expiresAt?: number) => ((expiresAt || 0) * 1000)

const getAppJWT = (user: User, account: Account): TAppJWT => {

  const resJwt:TAppJWT= {
    accessToken: account.access_token,
    accessTokenExpires: getAccessTokenExpires(account.expires_at),
    refreshToken: account.refresh_token,
    user,
  }

  return resJwt
}

const nextAuthHelper = {
  getAppJWT,
  getAccessTokenExpires,
}

export {
  nextAuthHelper,
}

export type {
  TAppJWT,
  TAppSession,
}

